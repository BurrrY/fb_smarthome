require "defaultFirmware"

--local SSID_PASSWORD = "bg4CLHj4rDxHRsqU"
cfg = {}
iptmp = config.IP
cfg.ip= iptmp
cfg.netmask=config.NETMASK
cfg.gateway=config.GATEWAY

  
wifi.sta.setip(cfg)
wifi.setmode (wifi.STATION)
wifi.sta.config (config.WIFISSID, config.WIFIPASS)
wifi.sta.autoconnect (1)

-- Hang out until we get a wifi connection before the httpd server is started.
wait_for_wifi_conn ( )
--launchWifiAP ()


-- Create the httpd server
svr = net.createServer (net.TCP, 30)

-- Server listening on port 80, call connect function if a request is received
svr:listen (80, connect)