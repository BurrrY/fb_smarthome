function measureSI7021(ADDR, commands, length)
    i2c.start(0)
    i2c.address(0, ADDR, i2c.TRANSMITTER)
    i2c.write(0, commands)
    i2c.stop(0)
    i2c.start(0)
    i2c.address(0, ADDR,i2c.RECEIVER)
    tmr.delay(20000)
    c = i2c.read(0, length)
    i2c.stop(0)
    
    return c
end

function readSI7021() 
    result = {}
    dataT = measureSI7021(0x40, 0xE3, 2)
    UT = string.byte(dataT, 1) * 256 + string.byte(dataT, 2)
    result.temp = ((UT*17572+65536/2)/65536 - 4685)
    
    dataH = measureSI7021(0x40, 0xE5, 2)
    UH = string.byte(dataH, 1) * 256 + string.byte(dataH, 2)
    result.humi = ((UH*12500+65536/2)/65536 - 600)

    return result
end