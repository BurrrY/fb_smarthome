function scani2c()
local gpio_pin= {4,3}
local id = 0;
local foundDevices = 0
local i2cDevices = {}

    for scl=1,7 do
         for sda=1,7 do
              tmr.wdclr() -- call this to pat the (watch)dog!
              if sda~=scl then -- if the pins are the same then skip this round
                   i2c.setup(id,sda,scl,i2c.SLOW) -- initialize i2c with our id and current pins in slow mode :-)
                   for i=0,127 do -- TODO - skip invalid addresses                     
                    
                        if find_i2c_dev(id, i)==true then
                        i2cDevices[foundDevices] = {}
                        i2cDevices[foundDevices]["addr"] = string.format("%02X",i);
                        i2cDevices[foundDevices]["sda"] = sda;
                        i2cDevices[foundDevices]["scl"] = scl;
                        foundDevices = foundDevices +1;
                        
                        print("Device found at address 0x"..string.format("%02X",i))
                        print("Device is wired: SDA to GPIO - IO index "..sda)
                        print("Device is wired: SCL to GPIO - IO index "..scl)
                        end
                   end
              end
         end
    end
    
    print("Scan Done.")
    return i2cDevices;
end


function find_i2c_dev(i2c_id, dev_addr)
     i2c.start(i2c_id)
     c=i2c.address(i2c_id, dev_addr ,i2c.TRANSMITTER)
     i2c.stop(i2c_id)
     return c
end
