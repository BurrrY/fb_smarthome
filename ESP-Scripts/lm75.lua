function readLM75(dev_addr)    
    i2c.setup(0, config.SDA, config.SCL, i2c.SLOW)    
    i2c.start(0)   
     
    FOUND_MODULE = i2c.address(0, dev_addr, i2c.RECEIVER)    
    bdata = i2c.read(0, 1)    
    i2c.stop(0)    
    
    return string.byte(bdata)
end
