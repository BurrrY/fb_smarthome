function get_http_post (instr)
   local t = {}
   local last

   for str in string.gmatch (instr, "([^\n]+)") do
      last = str
   end

    
    for str in string.gmatch (last, "([^&]+)") do    
        strt_ndx, end_ndx = string.find (str, "([^=]+)")
        if (end_ndx ~= nil) then
            key = trim (string.sub (str, strt_ndx, end_ndx))
            v = trim (string.sub (str, end_ndx+2))
            t[key] = v;
        end
    end

    return t
end

function get_http_req (instr)
   local t = {}
   local first = nil
   local key, v, strt_ndx, end_ndx

   for str in string.gmatch (instr, "([^\n]+)") do
      -- First line in the method and path
      if (first == nil) then
         first = 1
         strt_ndx, end_ndx = string.find (str, "([^ ]+)")
         v = trim (string.sub (str, end_ndx + 2))
         key = trim (string.sub (str, strt_ndx, end_ndx))
         t["METHOD"] = key
         t["REQUEST"] = v
      else -- Process and reamaining ":" fields
         strt_ndx, end_ndx = string.find (str, "([^:]+)")
         if (end_ndx ~= nil) then
            v = trim (string.sub (str, end_ndx + 2))
            key = trim (string.sub (str, strt_ndx, end_ndx))
            t[key] = v
         end
      end
   end

   return t
end
