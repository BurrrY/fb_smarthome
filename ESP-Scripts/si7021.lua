function measureSI7021(ADDR, commands, length)
    i2c.setup(0, config.SDA, config.SCL, i2c.SLOW)    
    i2c.start(0)
    FOUND_MODULE = i2c.address(0, ADDR, i2c.TRANSMITTER)
    i2c.write(0, commands)
    i2c.stop(0)
    i2c.start(0)
    i2c.address(0, ADDR,i2c.RECEIVER)
    tmr.delay(20000)
    c = i2c.read(0, length)
    i2c.stop(0)
    
    return c
end

function readSI7021() 
    result = {}
    dataT = measureSI7021(0x40, 0xE3, 2)
    UT = string.byte(dataT, 1) * 256 + string.byte(dataT, 2)
    result.temp = ((UT*17572+65536/2)/65536 - 4685)
    
    dataH = measureSI7021(0x40, 0xE5, 2)
    UH = string.byte(dataH, 1) * 256 + string.byte(dataH, 2)
    result.humi = ((UH*12500+65536/2)/65536 - 600)

    return result
end



function readSI7021Human() 
    t = readSI7021()
    
    t.temp = t.temp/100 .. "." .. string.sub(t.temp, -2) .. "°C"
    t.humi = t.humi/100 .. "." .. string.sub(t.humi, -2) .. "%"

    return t
end

--sens = readSI7021();
--print ("Temperature: " .. sens.temp/100 .. "." .. string.sub(sens.temp, -2) .. "°C")
--print ("Humidity: " .. sens.humi/100 .. "." .. string.sub(sens.humi, -2) .. "%")
