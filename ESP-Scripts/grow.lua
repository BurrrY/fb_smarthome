local uartBuff = "-";
local latestGrow = {}

function readGrow()
    result = {}
    result.temp = latestGrow.temp;
    result.light = latestGrow.light;
    result.heater = latestGrow.heater;
    result.humi= latestGrow.humi;
    result.time= latestGrow.time;

    return result
end
