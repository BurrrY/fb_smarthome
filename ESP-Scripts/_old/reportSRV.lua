-- Your Wifi connection data
local SSID = "Bury-Smarthome"
local SSID_PASSWORD = "bg4CLHj4rDxHRsqU"
local SERVER_IP = "192.168.178.150"
local NODE_IP = "192.168.178.122"
local NODE_ID = "7"
local SERVER_PORT = 8080

cfg =
  {
    ip=NODE_IP,
    netmask="255.255.255.0",
    gateway="192.168.178.1"
  }

  
local pin = 4
local state = 0

gpio.mode(pin, gpio.INPUT)
state = gpio.read(pin)

local function connect (conn, data)
   local query_data

   conn:on ("receive",
      function (cn, req_data)         
        print ("GPIO:"..state)
        cn:send ("GPIO:"..state)
         -- Close the connection for the request
         cn:close ()
      end)
end

function wait_for_wifi_conn ( )
   tmr.alarm (1, 1000, 1, function ( )
      if wifi.sta.status ( ) ~= 5 then
         print ("Waiting for Wifi connection: " .. wifi.sta.status ())
      else
         tmr.stop (1)
         print ("ESP8266 mode is: " .. wifi.getmode ( ))
         print ("WIFI-Status is: " .. wifi.sta.status ( ))
         print ("The module MAC address is: " .. wifi.ap.getmac ( ))
         print ("Config done, IP is " .. wifi.sta.getip ( ))
      end
   end)
end


-- String trim left and right
function trim (s)
  return (s:gsub ("^%s*(.-)%s*$", "%1"))
end

function checkPin()
    tmr.alarm (1, 100, 1, function ( )
    
         newPinState = gpio.read(pin)
         
         if newPinState ~= state then
            state = newPinState
            reportState(state)
         end
    end)
end

function reportState(s)
    url = "/changeState.html?id=".. NODE_ID .."&action=".. (state+3)
    req = "GET ".. url .." HTTP/1.1\r\nHost: 192.168.178.150:8080\r\nConnection: keep-alive\r\nAccept: */*\r\n\r\n"
    print(req)

    conn=net.createConnection(net.TCP, 0)
    conn:on("receive", function(sck, c) print(c) end )
    conn:on("connection", function(c)
        conn:send(req) 
        end)
        
    conn:connect(8080, "192.168.178.150")
end

-- Configure the ESP as a station (client)

  
wifi.sta.setip(cfg)
wifi.setmode (wifi.STATION)
wifi.sta.config (SSID, SSID_PASSWORD)
wifi.sta.autoconnect (1)

-- Hang out until we get a wifi connection before the httpd server is started.
wait_for_wifi_conn ( )


checkPin()


-- Create the httpd server
svr = net.createServer (net.TCP, 30)

-- Server listening on port 80, call connect function if a request is received
svr:listen (80, connect)