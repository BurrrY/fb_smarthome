-- Your Wifi connection data
local SSID = "Bury-Smarthome"
local SSID_PASSWORD = "bg4CLHj4rDxHRsqU"

local pin = 4
local state = 0
gpio.mode(pin, gpio.OUTPUT)

local function connect (conn, data)
   local query_data

   conn:on ("receive",
      function (cn, req_data)

         query_data = get_http_req (req_data)
         req = query_data.REQUEST
         print (query_data["METHOD"] .. " " .. req)

         if (string.sub(req, 0, 5) == "/gpio") then            
            if(string.sub(req, 9, 9) == "1") then
                state = gpio.HIGH
                gpio.write(pin,state)
            elseif(string.sub(req, 9, 9) == "0") then
                state = gpio.LOW
                gpio.write(pin,state)
            end            
         end
         
        print ("GPIO:"..state)
        cn:send ("GPIO:"..state)
         -- Close the connection for the request
         cn:close ()
      end)
end

function wait_for_wifi_conn ( )
   tmr.alarm (1, 1000, 1, function ( )
      if wifi.sta.status ( ) ~= 5 then
         print ("Waiting for Wifi connection: " .. wifi.sta.status ())
      else
         tmr.stop (1)
         print ("ESP8266 mode is: " .. wifi.getmode ( ))
         print ("WIFI-Status is: " .. wifi.sta.status ( ))
         print ("The module MAC address is: " .. wifi.ap.getmac ( ))
         print ("Config done, IP is " .. wifi.sta.getip ( ))
      end
   end)
end

-- Build and return a table of the http request data
function get_http_req (instr)
   local t = {}
   local first = nil
   local key, v, strt_ndx, end_ndx

   for str in string.gmatch (instr, "([^\n]+)") do
      -- First line in the method and path
      if (first == nil) then
         first = 1
         strt_ndx, end_ndx = string.find (str, "([^ ]+)")
         v = trim (string.sub (str, end_ndx + 2))
         key = trim (string.sub (str, strt_ndx, end_ndx))
         t["METHOD"] = key
         t["REQUEST"] = v
      else -- Process and reamaining ":" fields
         strt_ndx, end_ndx = string.find (str, "([^:]+)")
         if (end_ndx ~= nil) then
            v = trim (string.sub (str, end_ndx + 2))
            key = trim (string.sub (str, strt_ndx, end_ndx))
            t[key] = v
         end
      end
   end

   return t
end

-- String trim left and right
function trim (s)
  return (s:gsub ("^%s*(.-)%s*$", "%1"))
end

function ticktack()
    tmr.alarm (1, 1000, 1, function ( )
         print (",")
    end)
end

-- Configure the ESP as a station (client)
cfg =
  {
    ip="192.168.178.120",
    netmask="255.255.255.0",
    gateway="192.168.178.1"
  }
  
wifi.sta.setip(cfg)
wifi.setmode (wifi.STATION)
wifi.sta.config (SSID, SSID_PASSWORD)
wifi.sta.autoconnect (1)

-- Hang out until we get a wifi connection before the httpd server is started.
wait_for_wifi_conn ( )

-- Create the httpd server
svr = net.createServer (net.TCP, 30)

-- Server listening on port 80, call connect function if a request is received
svr:listen (80, connect)

