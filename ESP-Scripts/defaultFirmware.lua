require "config"
require "si7021"
require "lm75"
require "i2c_general"
require "httpParser"
require "wifiConnection"
require "webserver"
require "socket"
require "grow"
require "uart"

-- I2C-Data
FOUND_MODULE = true


-- "http://192.168.178.121:80/gpio/2/0"
local function connect (conn, data)
   local query_data
   local resp
   local head
   local rep

   conn:on ("receive",
      function (cn, req_data)

         query_data = get_http_req (req_data)
         req = query_data.REQUEST
         print (query_data["METHOD"] .. ": " .. req)

         head = "HTTP/1.0 200 OK\r\n\r\n"
         
         if (string.sub(req, 0, 5) == "/temp") then            
            addr = string.sub(req, 7, 10)
            temp = readLM75(addr)

            if(FOUND_MODULE==false) then
                t = {["err"] = "Cannot find Module"}
            else
                t = {["addr"] = addr, ["temp"] = temp, ["err"] = "no-error"}
            end  
                resp = cjson.encode(t);         
     --============================================       
         elseif (string.sub(req, 0, 7) == "/config") then    
            resp = readFile("config.html")
            resp = string.gsub(resp, "IP_VAL", config["IP"])
            resp = string.gsub(resp, "NETMASK_VAL", config["NETMASK"])
            resp = string.gsub(resp, "GATEWAY_VAL", config["GATEWAY"])
            resp = string.gsub(resp, "WIFISSID_VAL", config["WIFISSID"])
            resp = string.gsub(resp, "SDA_VAL", config["SDA"])
            resp = string.gsub(resp, "SCL_VAL", config["SCL"])
            
     --============================================       
         elseif (string.sub(req, 0, 10) == "/style.css") then     
            resp = readFile("style.css")
     --============================================       
         elseif (string.sub(req, 0, 11) == "/saveConfig") then            
            print("Write config")
            t = get_http_post(req_data)
            saveSettings(t)
            resp = "saved";
     --============================================       
         elseif (string.sub(req, 0, 8) == "/i2cscan") then                   
            resp = cjson.encode(scani2c());
     --============================================       
         elseif (string.sub(req, 0, 8) == "/si7021N") then            
            t = readSI7021Human();  
            
            if(FOUND_MODULE==false) then
                t = {}
                t.err = "Cannot find Module"
            else  
                t.err = "no-error" 
            end        
            
            resp = cjson.encode(t);
     --============================================       
         elseif (string.sub(req, 0, 7) == "/si7021") then
            t = readSI7021();
            
            if(FOUND_MODULE==false) then
                t = {}
                t.err = "Cannot find Module"
            else  
                t.err = "no-error" 
            end     
                 
            resp = cjson.encode(t);
     --============================================       
         elseif (string.sub(req, 0, 5) == "/grow") then
            t = readGrow();
               
                 
            resp = cjson.encode(t);
     --============================================       
         else
            t = {["err"] = "cannot-parse", ["reqr"] = req}
            resp = cjson.encode(t);
         end

         cn:send (head .. resp)
         cn:close ()
      end)
end

function saveSettings(dataTable) 
   local wifipasstmp
    
    wifipasstmp = config.WIFIPASS
    
    file.open("config.lua","w+");
    file.writeline('config = {}')
    
    for k,v in pairs(t) do
        if k == "WIFIPASS" then
            if v ~= "WIFIPASS_VAL" then 
                print("write:" .. k .." = " .. v )
                file.writeline("config[\"" .. k .. "\"] = \"" .. v .. "\"")
            else
                print("write:" .. k .." = " .. wifipasstmp )
                file.writeline("config[\"" .. k .. "\"] = \"" .. wifipasstmp .. "\"")
            end
        else
            print("write:" .. k .." = " .. v )
            file.writeline("config[\"" .. k .. "\"] = \"" .. v .. "\"")
        end
    end

    file.close()
    dofile("config.lua");
   return t
end

function trim (s)
  return (s:gsub ("^%s*(.-)%s*$", "%1"))
end

cfg = {}
cfg.ip= config.IP
cfg.netmask=config.NETMASK
cfg.gateway=config.GATEWAY

  
wifi.sta.setip(cfg)
wifi.setmode (wifi.STATION)
wifi.sta.config (config.WIFISSID, config.WIFIPASS)
wifi.sta.autoconnect (1)

wait_for_wifi_conn ()
--launchWifiAP ()


svr = net.createServer (net.TCP, 30)

svr:listen (80, connect)

print("OVERWRITING UART in 5 SECS! -> uart.lua")
print("OVERWRITING UART in 5 SECS! -> uart.lua")
if not tmr.alarm(0, 5000, tmr.ALARM_SINGLE, 
    function() 
        enableUART()
        print("UART-Overwrite done!")
    end) 
then 
    print("whoopsie enableUART()") 
end


