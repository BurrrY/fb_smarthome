

function wait_for_wifi_conn ( )
    local errCnt = 0
   tmr.alarm (1, 1000, 1, function ( )
      if wifi.sta.status ( ) ~= 5 then
        if wifi.sta.status() == 2 or wifi.sta.status() == 3 or wifi.sta.status() == 4 then
            errCnt = errCnt + 1;
        end 
        if(errCnt == 5) then
            tmr.stop (1)
           launchWifiAP () 
        end
            
         print ("Waiting for Wifi: " .. wifi.sta.status () .. " ErrCnt: " .. errCnt)
      else
         tmr.stop (1)
         print ("ESP8266 mode is: " .. wifi.getmode ( ))
         print ("WIFI-Status is: " .. wifi.sta.status ( ))
         print ("The module MAC address is: " .. wifi.ap.getmac ( ))
         print ("Config done, IP is " .. wifi.sta.getip ( ))
      end
   end)
end



function launchWifiAP ()
    local dhcp_config
    
    print("launch Wifi AP...");
    wifi.setmode (wifi.SOFTAP)

     cfg =
     {
         ssid="FB_SmartHome Node"
     }
     
     wifi.ap.config(cfg)

    cfg =
    {
        ip="192.168.178.1",
        netmask="255.255.255.0",
        gateway="192.168.178.1"
    }
    wifi.ap.setip(cfg)
    

    dhcp_config ={}
    dhcp_config.start = "192.168.178.100"
    wifi.ap.dhcp.config(dhcp_config)
    wifi.ap.dhcp.start()
end
