#ifndef MAINCLI_H
#define MAINCLI_H

#include <messagebus.h>

#include <QObject>
#include <QTimer>
#include <sh_db.h>
#include <webserver.h>

class MainCLI : public QObject
{
    Q_OBJECT
public:
    MainCLI();
    void execute();
private:
    Webserver *w;
    MessageBus *server;

    qint64 serverStart;

    //CHECKER
    QMap<int, SH_Node*> Nodes;
    sh_db *db;
    void reloadNodes();
signals:

};

#endif // MAINCLI_H
