#include "maincli.h"

#include <sh_node_OnOff.h>
#include <sh_node_lm75.h>
#include <sh_node_openclose.h>
#include <sh_node_s0.h>
#include <sh_node_si7021.h>
#include <sh_node_solar.h>


#include <QNetworkInterface>

MainCLI::MainCLI()
{

}

void MainCLI::execute()
{
    qSetMessagePattern("[%{time process} %{category} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{file}:%{line} - %{message}");

    qDebug() << "Starting MainCLI()";
    db = new sh_db();
    db->connnect("FB_Smarthome.db");
    sh_db::setInstance(db);


    qDebug() << "#";
    qDebug() << "#########################################";
    qDebug() << "#";

    db->checkDB();


    QNetworkInterface *inter = new QNetworkInterface();
    QList<QHostAddress> list;
    list=inter->allAddresses();
    QString str;

    qDebug() << "#";
    qDebug() << "#########################################";
    qDebug() << "#";
    qDebug() << "Webserver accessible under following IPs:";

    for (int i = 0; i < list.size(); ++i)
        qDebug() << (list.at(i).toString());

    qDebug() << "#";
    qDebug() << "#########################################";
    qDebug() << "#";

    qDebug() << "Starting...";

    reloadNodes();


    //Starting Webserver
    serverStart = QDateTime::currentMSecsSinceEpoch();
    w = new Webserver(db, serverStart);
    w->Nodes = &Nodes;

    //Starting Sockets-Server
    server = new MessageBus(85, true, this);

}




void MainCLI::reloadNodes()
{
    Nodes.clear();
    DB_DataSet d = db->getData("SELECT * from Nodes");


    for(DB_DataRow row : d) {
      
        QJsonDocument jDoc = QJsonDocument::fromJson(row.value("JsonConfig").toByteArray());
        QJsonObject jObj = jDoc.object();

        SH_Node *no = SH_Node::nodeFactory(row.value("Type").toInt(), db);
        no->setBasicInfo(row);
        no->setJsonConfig(jObj);
        Nodes.insert(no->getID(), no);
		
        if(no->getID() == 18) {
            Rule *r = new Rule();
            r->Condition.sourceNodeID = 19;
            r->Condition.Data = "on";
            r->Condition.setComparator("=");

            r->Action = SH_EventMessage(18, "on");

			no->addRule(r);
		}
    }
	
	for(SH_Node *n : Nodes) {
        for(Rule *r : n->getRules()) {
            int observedNodeID = r->Condition.sourceNodeID;
			connect(Nodes.value(observedNodeID), &SH_Node::changed, n, &SH_Node::receiveEvent);
		}
	}
}
