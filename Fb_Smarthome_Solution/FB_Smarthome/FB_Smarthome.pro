#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T19:03:40
#
#-------------------------------------------------

QT       += core gui network sql websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11
TARGET = FB_Smarthome
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle


OBJECTS_DIR=build
MOC_DIR=build

configJenkins {
    JKWORKSPACE=/var/lib/jenkins/workspace
    JKJOBS=/var/lib/jenkins/jobs

    FBSQL_ROOT=$${JKWORKSPACE}/FB_SQL/
    FBTS_ROOT=$${JKWORKSPACE}/FB_Timespan/
    FBSHLIB_ROOT=$${JKWORKSPACE}/FB_Smarthome/Fb_Smarthome_Solution

    LIBS += -L$${FBSQL_ROOT}/build -lFB_SQL
    INCLUDEPATH += $${FBSQL_ROOT}/FB_SQL
    DEPENDPATH += $${FBSQL_ROOT}/FB_SQL

    LIBS += -L$${FBTS_ROOT}/build -lFB_Timespan
    INCLUDEPATH += $${FBTS_ROOT}/FB_Timespan
    DEPENDPATH += $${FBTS_ROOT}/FB_Timespan


    LIBS += -L$${FBSHLIB_ROOT}/output -lFB_Timespan
    INCLUDEPATH += $${FBTS_ROOT}/FB_SH_Lib
    DEPENDPATH += $${FBTS_ROOT}/FB_SH_Lib
}


SOURCES += main.cpp\
    maincli.cpp

HEADERS  += \
    maincli.h

FORMS    +=

DISTFILES += \
    Serverpage/style.css \
    Serverpage/index.html \
    Serverpage/pageHead.html \
    Serverpage/pageFoot.html \
    Serverpage/light.html \
    Serverpage/tpl/tpl_OnOffNode.html \
    Serverpage/plugs.html \
    Serverpage/solar.html \
    Serverpage/Chart.min.js \
    Serverpage/jquery.min.js \
    Serverpage/openClose.html \
    Serverpage/tpl/tpl_OpenCloseNode.html \
    Serverpage/changeState.html \
    Serverpage/switchHistory.html \
    Serverpage/tpl/tpl_alertEntry.html \
    Serverpage/temp.html \
    Serverpage/tpl/tpl_longButton.html \
    Serverpage/tempDetail.html \
    Serverpage/server.html \
    Serverpage/apiTest.html \
    Serverpage/config.html \
    Serverpage/config/config.html


unix|win32: LIBS += -lFB_SQL
unix|win32: LIBS += -lFB_Timespan
unix|win32: LIBS += -lFB_SH_Lib

unix|win32: LIBS += -L$$DESTDIR/ -lFB_SQL
unix|win32: LIBS += -L$$DESTDIR/ -lFB_Timespan
unix|win32: LIBS += -L$$DESTDIR/ -lFB_SH_Lib

INCLUDEPATH += $$PWD/../../../fb_sql/FB_SQL
INCLUDEPATH += $$PWD/../../../fb_timespan/FB_Timespan
INCLUDEPATH += $$PWD/../FB_SH_Lib

DEPENDPATH += $$PWD/../../../fb_sql/FB_SQL
DEPENDPATH += $$PWD/../../../fb_timespan/FB_Timespan
DEPENDPATH += $$PWD/../FB_SH_Lib








