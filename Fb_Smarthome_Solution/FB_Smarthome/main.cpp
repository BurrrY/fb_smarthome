#include "maincli.h"
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MainCLI *c = new MainCLI;
    c->execute();

    return a.exec();
}
