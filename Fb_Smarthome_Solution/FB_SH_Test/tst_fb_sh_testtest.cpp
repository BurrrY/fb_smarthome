#include <QString>
#include <QtTest>
#include <QJsonObject>
#include <sh_node.h>
#include <sh_node_OnOff.h>
#include <sh_node_gpio.h>
#include <sh_node_lm75.h>
#include <sh_node_openclose.h>
#include <sh_node_s0.h>
#include <sh_node_si7021.h>
#include <sh_node_solar.h>

#include "sh_eventmessage.h"
#include "rulecondition.h"
#include "rule.h"
#include "sh_db.h"

class FB_SH_TestTest : public QObject
{
    Q_OBJECT

public:
    FB_SH_TestTest();
	sh_db *db;
	QList<SH_Node *> Nodes;

private Q_SLOTS:
    void EvMe_defaultConstructor();
    void EvMe_constructorSourceID_data();
    void EvMe_constructorSourceID();
    //void EvMe_constructorSourceIDInvalid_data();
//	void EvMe_constructorSourceIDInvalid();
//	void EvMe_constructorSourceIDValid_data();
//	void EvMe_constructorSourceIDValid();
//	void EvMe_constructorSRC_data();
//	void EvMe_constructorSRC();
	void EvMe_constructorData_data();
	void EvMe_constructorData();	
	
	void RuleCond_constrDefault();
    void RuleCond_constrSrcIDInvalid_data();
    void RuleCond_constrSrcIDInvalid();
    void RuleCond_constrSrcIDValid_data();
    void RuleCond_constrSrcIDValid();
    //void RuleCond_constrSrcID_data();
//	void RuleCond_constrSrcID();
	void RuleCond_constrCmpValid_data();
	void RuleCond_constrCmpValid();	
	void RuleCond_constrCmpInvalid_data();
	void RuleCond_constrCmpInvalid();	
	void RuleCond_constrData_data();
	void RuleCond_constrData();
	void RuleCond_verifyTrueStringsEqual_data();
	void RuleCond_verifyTrueStringsEqual();
	void RuleCond_verifyFalseStringsEqual_data();
	void RuleCond_verifyFalseStringsEqual();
	void RuleCond_SenderValid_data();
	void RuleCond_SenderValid();
	void RuleCond_SenderInvalid_data();
	void RuleCond_SenderInvalid();
	
	void Rule_constrDefault();
	void Rule_constrName_data();
	void Rule_constrName();
	
	void SH_NodesValid_data();
	void SH_NodesValid();
	void SH_NodesBasic();
	void SH_NodesFactory();	
	void SH_NodesSetJSON_data();
	void SH_NodesSetJSON();
	
	void SH_NodesGPIOSetJSON_data();
	void SH_NodesGPIOSetJSON();
	
	void Db_CreateTestDB();
};

FB_SH_TestTest::FB_SH_TestTest()
{
}

//####
//#   sh_eventmessage ###		
//####

void FB_SH_TestTest::EvMe_defaultConstructor(){	
	SH_EventMessage e1;
	QCOMPARE(e1.sender, -1);
	QCOMPARE(e1.source, ESC_None);
	QVERIFY(e1.timestamp.isValid());
    QCOMPARE(e1.timestamp.date(), QDateTime::currentDateTime().date());
    QCOMPARE(e1.parameter.size(), 0);
}

void FB_SH_TestTest::EvMe_constructorSourceID_data(){
    QTest::addColumn<int>("ID");
    QTest::addColumn<int>("Result");
	
    QTest::newRow("valid1") << 5 << 5;
    QTest::newRow("valid2") << 737 << 737;
    QTest::newRow("invalid1") << 0 << -1;
    QTest::newRow("invalid2") << -5 << -1;	
}
void FB_SH_TestTest::EvMe_constructorSourceID(){	
    QFETCH(int, ID);
    QFETCH(int, Result);
	
	SH_EventMessage e1(ID, "firstData");
	QCOMPARE(e1.sender, Result);
	QCOMPARE(e1.source, ESC_None);
	
	QVERIFY(e1.timestamp.isValid());
	QCOMPARE(e1.timestamp.date(), QDateTime::currentDateTime().date());
	
    QCOMPARE(e1.parameter.size(), 1);
    QCOMPARE(e1.parameter.contains("data"), 1);
    QCOMPARE(e1.parameter.value("data"), QString("firstData"));
}
/*

void FB_SH_TestTest::EvMe_constructorSRC_data(){

    QTest::addColumn<EventSourceComponent>("ESC");
	
    QTest::newRow("valid1") << ESC_Webserver;
    QTest::newRow("valid2") << ESC_GPIO;
    QTest::newRow("valid3") << ESC_Notify;
    QTest::newRow("valid4") << ESC_None;	
}
void FB_SH_TestTest::EvMe_constructorSRC(){	
    QFETCH(EventSourceComponent, ESC);
	
	SH_EventMessage e1(13, "firstData", ESC);
	QCOMPARE(e1.sender, 13);
	QCOMPARE(e1.source, ESC);
	
	QVERIFY(e1.timestamp.isValid());
	QCOMPARE(e1.timestamp.date(), QDateTime::currentDateTime().date());
	
    QCOMPARE(e1.parameter.size(), 1);
    QCOMPARE(e1.parameter.contains("data"), 1);
    QCOMPARE(e1.parameter.value("data"), "firstData");
}*/

void FB_SH_TestTest::EvMe_constructorData_data(){
    QTest::addColumn<QString>("d");
	
    QTest::newRow("valid1") << "test1";
    QTest::newRow("longer, newline") << "asdasd this is some veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. som \n newlines also!";
    QTest::newRow("umlaute") << "äööäü!\"&/() german umlaute and spcial chars";
    QTest::newRow("Empty") << "";	
}
void FB_SH_TestTest::EvMe_constructorData(){	
    QFETCH(QString, d);
	
	SH_EventMessage e1(13, d);
	QCOMPARE(e1.sender, 13);
	
	QVERIFY(e1.timestamp.isValid());
	QCOMPARE(e1.timestamp.date(), QDateTime::currentDateTime().date());
	
    QCOMPARE(e1.parameter.size(), 1);
    QCOMPARE(e1.parameter.contains("data"), 1);
    QCOMPARE(e1.parameter.value("data"), d);
}

//####
//#   RULE-CONDITIONS ###		
//####


void FB_SH_TestTest::RuleCond_constrDefault(){
	RuleCondition rc1;
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString(""));
	QCOMPARE(rc1.sourceNodeID, -1);
}

void FB_SH_TestTest::RuleCond_constrSrcIDInvalid_data(){
	QTest::addColumn<int>("ID");
    QTest::addColumn<int>("Result");

    QTest::newRow("invalid1") << 0 << -1;
    QTest::newRow("invalid1") << -1 << -1;
    QTest::newRow("invalid2") << -5523 << -1;	
}
void FB_SH_TestTest::RuleCond_constrSrcIDInvalid(){
	QFETCH(int, ID);
    QFETCH(int, Result);	
	
	RuleCondition rc1(ID, "=", "-");
	QCOMPARE(rc1.sourceNodeID, Result);
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString("-"));
	
	
	SH_EventMessage e1(ID, "-");
	(rc1.isValid(e1));
    QCOMPARE(rc1.getCompareResult(), QString("SourceNodeID invalid."));
}

void FB_SH_TestTest::RuleCond_constrSrcIDValid_data(){
	QTest::addColumn<int>("ID");
    QTest::addColumn<int>("Result");
	
    QTest::newRow("valid1") << 5 << 5;
    QTest::newRow("valid2") << 737 << 737;	
}
void FB_SH_TestTest::RuleCond_constrSrcIDValid(){
	QFETCH(int, ID);
    QFETCH(int, Result);	
	
	RuleCondition rc1(ID, "=", "-");
	QCOMPARE(rc1.sourceNodeID, Result);
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString("-"));
	
	
	SH_EventMessage e1(ID, "-");
	QVERIFY(rc1.isValid(e1));
	QVERIFY(rc1.getCompareResult() != "SourceNodeID invalid.");	
}

void FB_SH_TestTest::RuleCond_constrCmpValid_data(){
	QTest::addColumn<QString>("Input");
    QTest::addColumn<QString>("Output");
    QTest::addColumn<QString>("CompareRes");
	
    QTest::newRow("valid1") << "=" << "=" << "";
    QTest::newRow("valid2") << "<" << "<" << "";
    QTest::newRow("valid3") << ">" << ">" << "";
}
void FB_SH_TestTest::RuleCond_constrCmpValid(){
	QFETCH(QString, Input);
    QFETCH(QString, Output);
    QFETCH(QString, CompareRes);	
	
	RuleCondition rc1(1, Input, "testString");
	QCOMPARE(rc1.sourceNodeID, 1);
    QCOMPARE(rc1.getComparator(), Output);
    QCOMPARE(rc1.Data, QString("testString"));
	
	SH_EventMessage e1(1, "testString");
	rc1.isValid(e1);
	QVERIFY(rc1.getCompareResult() != "Comparator empty.");	
}

void FB_SH_TestTest::RuleCond_constrCmpInvalid_data(){
	QTest::addColumn<QString>("Input");
    QTest::addColumn<QString>("Output");
	
    QTest::newRow("invalid2") << "F" << "";
    QTest::newRow("invalid3") << "" << "";
    QTest::newRow("invalid4") << "<=" << "";	
    QTest::newRow("invalid5") << "==" << "";	
}
void FB_SH_TestTest::RuleCond_constrCmpInvalid(){
	QFETCH(QString, Input);
    QFETCH(QString, Output);	
	
    RuleCondition rc1(1, Input, "testString2");
	QCOMPARE(rc1.sourceNodeID, 1);
    QCOMPARE(rc1.getComparator(), Output);
    QCOMPARE(rc1.Data, QString("testString2"));
	
	SH_EventMessage e1(1, "testString");
	//When invalid Comparator is set, it has to be empy an result is false
	QVERIFY(!rc1.isValid(e1));
    QCOMPARE(rc1.getCompareResult(), QString("Comparator empty."));
}

void FB_SH_TestTest::RuleCond_constrData_data(){
	QTest::addColumn<QString>("Input");
	
    QTest::newRow("valid1") << "test1";
    QTest::newRow("longer, newline") << "asdasd this is some veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. eeeeeeeee eeeeeeeee eeeeeeeeeeee eeeeerx lon text. some \n newlines also!";
    QTest::newRow("umlaute") << "äööäü!\"&/() german umlaute and spcial chars";
    QTest::newRow("Empty") << "";	
    QTest::newRow("Space") << " ";		
}
void FB_SH_TestTest::RuleCond_constrData(){
	QFETCH(QString, Input);	
	
	RuleCondition rc1(1, "=", Input);
	QCOMPARE(rc1.sourceNodeID, 1);
    QCOMPARE(rc1.getComparator(), QString("="));
	QCOMPARE(rc1.Data, Input);
}

void FB_SH_TestTest::RuleCond_verifyTrueStringsEqual_data(){
	QTest::addColumn<QString>("Input");
    QTest::addColumn<QString>("Output");
	
    QTest::newRow("valid1") << "Test" << "Condition valid";
    QTest::newRow("validWithSpace") << " Test " << "Condition valid";
    QTest::newRow("valid2") << "test1" << "Condition valid";
    QTest::newRow("longer, newline") << "asdasd this is some veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. eeeeeeeee eeeeeeeee eeeeeeeeeeee eeeeerx lon text. some \n newlines also!" << "Condition valid";
    QTest::newRow("umlaute") << "äööäü!\"&/() german umlaute and spcial chars" << "Condition valid";
    QTest::newRow("Empty") << "" << "Condition valid";	
    QTest::newRow("Space") << " " << "Condition valid";	
    QTest::newRow("Tab") << "\t" << "Condition valid";	
}
void FB_SH_TestTest::RuleCond_verifyTrueStringsEqual(){
	QFETCH(QString, Input);
    QFETCH(QString, Output);	
	
	RuleCondition rc1(1, "=", Input);
	QCOMPARE(rc1.sourceNodeID, 1);
    QCOMPARE(rc1.getComparator(), QString("="));
	QCOMPARE(rc1.Data, Input);
	
	
	SH_EventMessage e1(1, Input);
	QVERIFY(rc1.isValid(e1));
	QCOMPARE(rc1.getCompareResult(), Output);	
}

void FB_SH_TestTest::RuleCond_verifyFalseStringsEqual_data(){
	QTest::addColumn<QString>("Input");
    QTest::addColumn<QString>("Output");
	
    QTest::newRow("valid1") << "Test" << "Data mismatch.";	
    QTest::newRow("validWithSpace") << " Test " << "Data mismatch.";	
    QTest::newRow("valid2") << "Data mismatch." << "Data mismatch.";	
    QTest::newRow("longer, newline") << "asdasd this is some veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. eeeeeeeee eeeeeeeee eeeeeeeeeeee eeeeerx lon text. some \n newlines also!" << "Data mismatch.";	
    QTest::newRow("umlaute") << "äööäü!\"&/() german umlaute and spcial chars" << "Data mismatch.";	
    QTest::newRow("Empty") << "" << "Data mismatch.";	
    QTest::newRow("Space") << " " << "Data mismatch.";	
    QTest::newRow("Tab") << "\t" << "Data mismatch.";	
}
void FB_SH_TestTest::RuleCond_verifyFalseStringsEqual(){
	QFETCH(QString, Input);
    QFETCH(QString, Output);	
	
	RuleCondition rc1(1, "=", "asdas");
	QCOMPARE(rc1.sourceNodeID, 1);
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString("asdas"));
	
	
	SH_EventMessage e1(1, Input);
	QVERIFY(!rc1.isValid(e1));
	QCOMPARE(rc1.getCompareResult(), Output);	
}


void FB_SH_TestTest::RuleCond_SenderValid_data(){
	QTest::addColumn<int>("Sender");
    QTest::addColumn<int>("Receiver");
    QTest::addColumn<QString>("Output");
	
    QTest::newRow("valid1") << 	1 << 1 << "";
    QTest::newRow("valid2") << 	56 << 56 << "";
}
void FB_SH_TestTest::RuleCond_SenderValid(){
	QFETCH(int, Sender);
    QFETCH(int, Receiver);	
    QFETCH(QString, Output);	
	
	RuleCondition rc1(Receiver, "=", "asdas");
	QCOMPARE(rc1.sourceNodeID, Receiver);
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString("asdas"));
	
	
	SH_EventMessage e1(Sender, "asdas");
    //QCOMPARE(rc1.sourceNodeID, e1.source);
	
	(rc1.isValid(e1));
	QVERIFY(rc1.getCompareResult() != "Sender/Source mismatch.");	
}

void FB_SH_TestTest::RuleCond_SenderInvalid_data(){
	QTest::addColumn<int>("Sender");
    QTest::addColumn<int>("Receiver");
	
    QTest::newRow("valid1") << 	1 << 421;
    QTest::newRow("valid2") << 	52 << 56;
    QTest::newRow("valid3") << 	-5 << 56;
    QTest::newRow("valid4") << 	52 << 5;
}
void FB_SH_TestTest::RuleCond_SenderInvalid(){
	QFETCH(int, Sender);
    QFETCH(int, Receiver);
	
	RuleCondition rc1(Receiver, "=", "asdas");
	QCOMPARE(rc1.sourceNodeID, Receiver);
    QCOMPARE(rc1.getComparator(), QString("="));
    QCOMPARE(rc1.Data, QString("asdas"));
	
	
	SH_EventMessage e1(Sender, "asdas");
    //QCOMPARE(rc1.sourceNodeID, e1.source);
	
	QVERIFY(!rc1.isValid(e1));
    QCOMPARE(rc1.getCompareResult(), QString("Sender/Source mismatch."));
}

//####
//#   RULES ###		
//####
void FB_SH_TestTest::Rule_constrDefault() {
    Rule r1;
    QCOMPARE(r1.Name, QString(""));
	r1.Name = "Test as";
    QCOMPARE(r1.Name, QString("Test as"));

    SH_EventMessage e1;
    e1 = r1.Action;
	QCOMPARE(e1.sender, -1);
	QCOMPARE(e1.source, ESC_None);
	QVERIFY(e1.timestamp.isValid());
    QCOMPARE(e1.timestamp.date(), QDateTime::currentDateTime().date());
 //   QCOMPARE(e1.parameter.size(), 0);
	
	
}
void FB_SH_TestTest::Rule_constrName_data(){
    QTest::addColumn<QString>("d");
	
    QTest::newRow("valid1") << "test1";
    QTest::newRow("longer, newline") << "asdasd this is some veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerx lon text. eeeeeeeee eeeeeeeee eeeeeeeeeeee eeeeerx lon text. some \n newlines also!";
    QTest::newRow("umlaute") << "äööäü!\"&/() german umlaute and spcial chars";
    QTest::newRow("Empty") << "";	
    QTest::newRow("Space") << " ";	
}
void FB_SH_TestTest::Rule_constrName(){	
    QFETCH(QString, d);
	
	Rule r1(d);
	QCOMPARE(r1.Name, d);
}

//####
//#   SH_Nodes ###		
//####

void FB_SH_TestTest::SH_NodesValid_data() {
	
    QTest::addColumn<int>("id");
    QTest::addColumn<QString>("name");
    QTest::addColumn<bool>("show");
    QTest::addColumn<QString>("cat");
	
	
	
    QTest::newRow("valid") << 2 << "TestName1" << true << "setHomeCategory7";
    QTest::newRow("valid2") << 2 << "TestName2" << false << "setHomeCategory6";
    QTest::newRow("valid3") << 2 << "TestName3" << true << "setHomeCategory5";
    QTest::newRow("valid4") << 2 << "TestName4" << false << "setHomeCategory4";
    QTest::newRow("valid5") << 2 << "TestName5" << true << "setHomeCategory3";
    QTest::newRow("valid6") << 2 << "TestName6" << false << "setHomeCategory2";
	
} 
void FB_SH_TestTest::SH_NodesValid() {
    QFETCH(int, id);
    QFETCH(QString, name);
    QFETCH(bool, show);
    QFETCH(QString, cat);
	
	SH_Node n;
	
	n.setName(name);
    QCOMPARE(n.getName(), name);
	
	n.setID(id);
    QCOMPARE(n.getID(), id);
	
    QVERIFY(n.getRules().size() == 0);
	Rule r;
    n.addRule(&r);
    QVERIFY(n.getRules().size() == 1);
	
	
	n.setShowOnHome(show);
    QCOMPARE(n.getShowOnHome(), show);
	
	n.setHomeCategory(cat);
    QCOMPARE(n.getHomeCategory(), cat);
	
	
}
void FB_SH_TestTest::SH_NodesBasic() {
	SH_Node s;
	
	QStringList *NodeTypeList = SH_Node::NodeTypes;	
    QVERIFY(NodeTypeList->size()==10);
	
    QVERIFY(s.getEventList().size()==0);
}
void FB_SH_TestTest::SH_NodesFactory() {
	sh_db testDb;
    SH_Node *n = SH_Node::nodeFactory(SH_Node::TypeID("Light"), &testDb);
    QVERIFY(dynamic_cast<SH_Node_OnOff *>(n));
    delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("PowerPlug"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_OnOff *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("OpenClose"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_OpenClose *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("Temp-Sens (LM75)"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_LM75 *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("Env-Sens (SI7021)"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_SI7021 *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("Solar"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_Solar *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("S0-Sensor"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_S0 *>(n));
	delete n;
	
    n = SH_Node::nodeFactory(SH_Node::TypeID("RPi - GPIO"), &testDb);
	QVERIFY(dynamic_cast<SH_Node_GPIO *>(n));
	delete n;

}
void FB_SH_TestTest::SH_NodesSetJSON_data() {
	
    QTest::addColumn<QJsonObject>("jObj");



    QJsonObject o { {"nodeName", "nodeName"}, {"nodeTypeDropdown", 2}, {"ShowOnHome", true}, {"homeCategory", "homeCategory"} };
    QTest::newRow("valid1") << o;

    QJsonObject o1 { {"nodeName", "nodeName"}, {"nodeTypeDropdown", 2}, {"ShowOnHome", true}, {"homeCategory", "homeCategory"} };
    QTest::newRow("valid") << o1;

    QJsonObject o2 { {"nodeName", "nodeName1"}, {"nodeTypeDropdown", 3}, {"ShowOnHome", false}, {"homeCategory", "homeCategory1"} };
    QTest::newRow("valid2") << o2;

    QJsonObject o3 { {"nodeName", "nodeName2"}, {"nodeTypeDropdown", 4}, {"ShowOnHome", true}, {"homeCategory", "homeCategory2"} };
    QTest::newRow("valid3") << o3;

    QJsonObject o4 { {"nodeName", "nodeName3"}, {"nodeTypeDropdown", 5}, {"ShowOnHome", false}, {"homeCategory", "homeCategory3"} };
    QTest::newRow("valid4") << o4;

    QJsonObject o5 { {"nodeName", "nodeName4"}, {"nodeTypeDropdown", 6}, {"ShowOnHome", true}, {"homeCategory", "homeCategory4"} };
    QTest::newRow("valid5") << o5;
}
void FB_SH_TestTest::SH_NodesSetJSON() {
	QFETCH(QJsonObject, jObj);
	
	SH_Node n1;	
	n1.setJsonConfig(jObj);

	QCOMPARE(n1.getHomeCategory(), jObj.value("homeCategory").toString());
	QCOMPARE(n1.getShowOnHome(), jObj.value("ShowOnHome").toBool());
	QCOMPARE(n1.getName(), jObj.value("nodeName").toString());
    QCOMPARE(n1.getTypeID(), jObj.value("nodeTypeDropdown").toInt());


	QJsonObject retVal = n1.getJsonConfig();
    QVERIFY(retVal.contains("homeCategory"));
    QVERIFY(retVal.contains("ShowOnHome"));
    QVERIFY(retVal.length() == 2);
	QCOMPARE(retVal.value("homeCategory").toString(), jObj.value("homeCategory").toString());
	QCOMPARE(retVal.value("ShowOnHome").toBool(), jObj.value("ShowOnHome").toBool());
}

//####
//#   SH_Nodes GPIO ###		
//####
void FB_SH_TestTest::SH_NodesGPIOSetJSON_data() {
	
    QTest::addColumn<QJsonObject>("jObj");
	

    QJsonObject o { {"nodeName", "nodeName"}, {"nodeTypeDropdown", 2}, {"ShowOnHome", true}, {"homeCategory", "homeCategory"},
                    {"PinNo", 11}, {"PinCheckInterval", 14}, {"PinInput", true} };
    QTest::newRow("valid1") << o;

    QJsonObject o1 { {"nodeName", "nodeName"}, {"nodeTypeDropdown", 2}, {"ShowOnHome", true}, {"homeCategory", "homeCategory"},
                     {"PinNo", 12}, {"PinCheckInterval", 15}, {"PinInput", false} };
    QTest::newRow("valid") << o1;

    QJsonObject o2 { {"nodeName", "nodeName1"}, {"nodeTypeDropdown", 3}, {"ShowOnHome", false}, {"homeCategory", "homeCategory1"},
                     {"PinNo", 13}, {"PinCheckInterval", 16}, {"PinInput", true} };
    QTest::newRow("valid2") << o2;

    QJsonObject o3 { {"nodeName", "nodeName2"}, {"nodeTypeDropdown", 4}, {"ShowOnHome", true}, {"homeCategory", "homeCategory2"},
                     {"PinNo", 14}, {"PinCheckInterval", 17}, {"PinInput", false} };
    QTest::newRow("valid3") << o3;

    QJsonObject o4 { {"nodeName", "nodeName3"}, {"nodeTypeDropdown", 5}, {"ShowOnHome", false}, {"homeCategory", "homeCategory3"},
                     {"PinNo", 15}, {"PinCheckInterval", 18}, {"PinInput", true} };
    QTest::newRow("valid4") << o4;

    QJsonObject o5 { {"nodeName", "nodeName4"}, {"nodeTypeDropdown", 6}, {"ShowOnHome", true}, {"homeCategory", "homeCategory4"},
                     {"PinNo", 16}, {"PinCheckInterval", 19}, {"PinInput", false} };
    QTest::newRow("valid5") << o5;

}
void FB_SH_TestTest::SH_NodesGPIOSetJSON() {
	QFETCH(QJsonObject, jObj);
	
	SH_Node_GPIO n1;	
	n1.setJsonConfig(jObj);

	QCOMPARE(n1.getHomeCategory(), jObj.value("homeCategory").toString());
	QCOMPARE(n1.getShowOnHome(), jObj.value("ShowOnHome").toBool());
	QCOMPARE(n1.getName(), jObj.value("nodeName").toString());
    QCOMPARE(n1.getTypeID(), jObj.value("nodeTypeDropdown").toInt());
	QCOMPARE(n1.getPin(), jObj.value("PinNo").toInt());
    QCOMPARE(n1.getInputCheckInterval(), jObj.value("PinCheckInterval").toInt());
	QCOMPARE(n1.getInput(), jObj.value("PinInput").toBool());


	QJsonObject retVal = n1.getJsonConfig();
    QVERIFY(retVal.contains("homeCategory"));
    QVERIFY(retVal.contains("ShowOnHome"));
    QVERIFY(retVal.contains("PinNo"));
    QVERIFY(retVal.contains("PinCheckInterval"));
    QVERIFY(retVal.contains("PinInput"));
    QVERIFY(retVal.length() == 5);
	QCOMPARE(retVal.value("homeCategory").toString(), jObj.value("homeCategory").toString());
	QCOMPARE(retVal.value("ShowOnHome").toBool(), jObj.value("ShowOnHome").toBool());
	QCOMPARE(retVal.value("PinNo"), jObj.value("PinNo"));
	QCOMPARE(retVal.value("PinCheckInterval"), jObj.value("PinCheckInterval"));
	QCOMPARE(retVal.value("PinInput"), jObj.value("PinInput"));
}




//####
//#   SH_DB ###		
//####
void FB_SH_TestTest::Db_CreateTestDB()
{	
	if(QFile::exists("FB_Smarthome_Test.db"))
		QFile::remove("FB_Smarthome_Test.db");
	
	
    db = new sh_db();
    sh_db::setInstance(db);
    QVERIFY2(db->connnect("FB_Smarthome_Test.db"), ("Could not connect to Test-DB!"));
    QVERIFY2(db->checkDB(), ("Error checking and creating Database!"));
	
	//TODO: Check if all Tables are created and stuff.
}



QTEST_APPLESS_MAIN(FB_SH_TestTest)

#include "tst_fb_sh_testtest.moc"
