#-------------------------------------------------
#
# Project created by QtCreator 2016-12-07T21:30:27
#
#-------------------------------------------------

QT       += testlib
QT       +=  network sql websockets

QT       -= gui

QMAKE_CXXFLAGS += -std=c++11
TARGET = tst_fb_sh_testtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_fb_sh_testtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"



configJenkins {
    JKWORKSPACE=/var/lib/jenkins/workspace
    JKJOBS=/var/lib/jenkins/jobs

    FBSQL_ROOT=$${JKWORKSPACE}/FB_SQL/
    FBTS_ROOT=$${JKWORKSPACE}/FB_Timespan/
    FBSHLIB_ROOT=$${JKWORKSPACE}/FB_Smarthome/Fb_Smarthome_Solution

    LIBS += -L$${FBSQL_ROOT}/build -lFB_SQL
    INCLUDEPATH += $${FBSQL_ROOT}/FB_SQL
    DEPENDPATH += $${FBSQL_ROOT}/FB_SQL

    LIBS += -L$${FBTS_ROOT}/build -lFB_Timespan
    INCLUDEPATH += $${FBTS_ROOT}/FB_Timespan
    DEPENDPATH += $${FBTS_ROOT}/FB_Timespan


    LIBS += -L$${FBSHLIB_ROOT}/output -lFB_Timespan
    INCLUDEPATH += $${FBTS_ROOT}/FB_SH_Lib
    DEPENDPATH += $${FBTS_ROOT}/FB_SH_Lib
}


unix|win32: LIBS += -lFB_SQL
unix|win32: LIBS += -lFB_Timespan
unix|win32: LIBS += -lFB_SH_Lib

unix|win32: LIBS += -L$$DESTDIR/ -lFB_SQL
unix|win32: LIBS += -L$$DESTDIR/ -lFB_Timespan
unix|win32: LIBS += -L$$DESTDIR/ -lFB_SH_Lib

INCLUDEPATH += $$PWD/../../../fb_sql/FB_SQL
INCLUDEPATH += $$PWD/../../../fb_timespan/FB_Timespan
INCLUDEPATH += $$PWD/../FB_SH_Lib

DEPENDPATH += $$PWD/../../../fb_sql/FB_SQL
DEPENDPATH += $$PWD/../../../fb_timespan/FB_Timespan
DEPENDPATH += $$PWD/../FB_SH_Lib

