#ifndef SH_NODE_WEB_H
#define SH_NODE_WEB_H
#include "fb_sh_lib_global.h"

#include "sh_node.h"

#include <QHostAddress>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>



class FB_SH_LIBSHARED_EXPORT  SH_Node_Network : public SH_Node
{
 Q_OBJECT
public:
    SH_Node_Network();
    SH_Node_Network(sh_db *db);


    QHostAddress getIP() const;

    void setIP(const QHostAddress &value);
    void setIP(const QString &value);
    void setIP(const quint32 &value);

    int getPort() const;
    void setPort(int value);

    // Virtual Methods
    virtual QJsonObject getJsonConfig();
    virtual void setJsonConfig(QJsonObject jsonData);
protected:
    QNetworkAccessManager nwManager;
    QNetworkRequest *nwRequest;

    QHostAddress IP;
    int Port;

  //  bool fetchData();

    bool fetchData(QUrl url);
    QNetworkReply *requestReply;
    int createNode();


    static const QString NODE_CMD_ON;
    static const QString NODE_CMD_OFF;
    static const QString NODE_GET_STATE;
public slots:
    void managerFinished(QNetworkReply *reply);
};

#endif // SH_NODE_WEB_H
