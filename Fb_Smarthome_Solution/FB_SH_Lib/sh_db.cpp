#include "sh_db.h"

#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include "QVariant"

sh_db::sh_db()
{

}




DB_DataSet sh_db::loadSwitchHistory(int NodeID) {
    qCInfo(shLibDB) << "calling loadSwitchHistory";
	
	if(NodeID<=0) {
		qCDebug(shLibDB) << "called loadSwitchHistory with invalid NodeID!: " << NodeID;
        qCInfo(shLibDB) << "checked: NodeID<=0";
        DB_DataSet b;
        return b;
	}

    QSqlQuery q;
    q.prepare("SELECT * FROM SwitchHistory WHERE NodeID = :ID ORDER BY timestamp ASC");
    q.bindValue(":ID", NodeID);
    return getData(q);
}


bool sh_db::createNode(QString Name, int typeID) {
    qCInfo(shLibDB) << "calling createNode";
	
	
	if(Name.isEmpty() || typeID < 0) {
		qCDebug(shLibDB) << "called createNode with invalid Paramter: " << Name << typeID;
		qCInfo(shLibDB) << "checked: Name.isEmpty() || typeID < 0";
		return false;
	}

    QSqlQuery q(db);
    q.prepare("INSERT INTO nodes (Name, Type) VALUES (:Name, :Type);");
    q.bindValue(":Name", Name);
    q.bindValue(":Type", typeID);
    return tryExecute(&q);
}

bool sh_db::insertSolarData(int pwr, int ID) {
    qCInfo(shLibDB) << "calling insertSolarData";
	
	
	if(ID < 0 || pwr < 0) {
		qCDebug(shLibDB) << "called insertSolarData with invalid Paramter: " << pwr << ID;
		qCInfo(shLibDB) << "checked: ID < 0 || pwr < 0";
		return false;
	}

    QSqlQuery q;
    q.prepare("INSERT INTO SolarLog (Timestamp, W_Total, Source) VALUES (:Timestamp, :PWR, :ID);");
    q.bindValue(":Timestamp", SQL_DATEFORMAT(QDateTime::currentDateTime()));
    q.bindValue(":PWR", pwr);
    q.bindValue(":ID", ID);
    return tryExecute(&q);
}


bool sh_db::updateNode(QString Name, int NodeID, int type, QJsonObject jsonData ) {
    qCInfo(shLibDB) << "calling updateNode";
	
	
	if(Name.isEmpty() || NodeID <= 0) {
		qCDebug(shLibDB) << "called updateNode with invalid Paramter: " << Name << NodeID;
		qCInfo(shLibDB) << "checked: Name.isEmpty() || NodeID <= 0";
		return false;
	}

    QSqlQuery q(db);
    q.prepare("UPDATE nodes SET Name = :Name, Type = :nodeType, JsonConfig = :JsonConfig WHERE ID = :ID;");
    q.bindValue(":Name", Name);
    q.bindValue(":ID", NodeID);
    q.bindValue(":nodeType", type);
    q.bindValue(":JsonConfig", QJsonDocument(jsonData).toJson());

    return tryExecute(&q);
}

bool sh_db::insertSwitchHistory(int nodeID, int State) {
    qCInfo(shLibDB) << "calling insertSwitchHistory";

	if(nodeID <= 0) {
        qCDebug(shLibDB) << "called insertSwitchHistory with invalid Paramter: " << nodeID << State;
		qCInfo(shLibDB) << "checked: nodeID <= 0";
		return false;
	}

	
    QSqlQuery q;
    q.prepare("INSERT INTO SwitchHistory (State, NodeID, timestamp) VALUES (:state, :nodeID, :timestamp)");
    q.bindValue(":timestamp", SQL_DATEFORMAT(QDateTime::currentDateTime()));
    q.bindValue(":state", (int)State);
    q.bindValue(":nodeID", nodeID);

    return tryExecute(&q);
}

bool sh_db::saveTemp(int val, int nodeID) {
    qCInfo(shLibDB) << "calling saveTemp";
	
	
	if(nodeID <= 0 || val > 999 || val < -999) {
		qCDebug(shLibDB) << "called saveTemp with invalid Paramter: " << nodeID << val;
		qCInfo(shLibDB) << "checked: nodeID <= 0 || val > 999 || val < -999";
		return false;
	}


    QSqlQuery q;
    q.prepare("INSERT INTO TempLog (NodeID, Value, Timestamp) VALUES (:NodeID, :Value, :Timestamp)");
    q.bindValue(":Timestamp", SQL_DATEFORMAT(QDateTime::currentDateTime()));
    q.bindValue(":Value", val);
    q.bindValue(":NodeID", nodeID);

    return tryExecute(&q);
}

bool sh_db::checkDB() {
    qCInfo(shLibDB) << "calling checkDB";

    #include "dbschema.h"
    return updateDB(DBSchema);
}

void sh_db::setInstance(sh_db *value) {
    qCInfo(shLibDB) << "calling setInstance";

    if(!FB_SQL::isOpen)
        qCWarning(shLibDB) << "Database is not open yet!";

    Instance = value;
}

sh_db *sh_db::getInstance() {
    qCInfo(shLibDB) << "calling getInstance";

    return (sh_db *)Instance;
}

bool sh_db::insertAlert(int nodeID, int level, QString msg) {
    qCInfo(shLibDB) << "calling insertAlert";
	
	
	if(nodeID <= 0 || msg.isEmpty()) {
		qCDebug(shLibDB) << "called insertAlert with invalid Paramter: " << nodeID << msg;
		qCInfo(shLibDB) << "checked: nodeID <= 0 || msg.isEmpty()";
		return false;
	}

    QSqlQuery q;
    q.prepare("INSERT INTO Alerts (Level, NodeID, DateTime, Message) VALUES (:level, :nodeID, :timestamp, :msg)");
    q.bindValue(":timestamp", SQL_DATEFORMAT(QDateTime::currentDateTime()));
    q.bindValue(":level", level);
    q.bindValue(":msg", msg);
    q.bindValue(":nodeID", nodeID);
	
    return tryExecute(&q);
}

bool sh_db::saveSI7021(int tempVal, int humiVal, int nodeID) {
    qCInfo(shLibDB) << "calling saveSI7021";

    if(nodeID <= 0 ) {
		qCDebug(shLibDB) << "called saveSI7021 with invalid Paramter: " << nodeID;
		qCInfo(shLibDB) << "checked: nodeID <= 0 ";
		return false;
	}
	
    QSqlQuery q;
    q.prepare("INSERT INTO SI7021Log (NodeID, TempValue, HumiValue, Timestamp) VALUES (:NodeID, :Temp, :Humi, :Timestamp)");
    q.bindValue(":Timestamp", SQL_DATEFORMAT(QDateTime::currentDateTime()));
    q.bindValue(":Temp", tempVal);
    q.bindValue(":Humi", humiVal);
    q.bindValue(":NodeID", nodeID);

    return tryExecute(&q);
}

