#ifndef SH_NODE_ONOFF_H
#define SH_NODE_ONOFF_H
#include "fb_sh_lib_global.h"

#include "sh_node_network.h"

#include <QTcpSocket>


class FB_SH_LIBSHARED_EXPORT  SH_Node_OnOff : public SH_Node_Network
{
public:
    SH_Node_OnOff(sh_db *db);

    ~SH_Node_OnOff();

	
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);
public slots:
    virtual void receiveEvent(SH_EventMessage em);
private:
    QTcpSocket socketConnection;
    bool tryConnect();
};
#endif // SH_NODE_ONOFF_H
