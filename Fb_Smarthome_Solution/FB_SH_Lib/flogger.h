#ifndef FLOGGER_H
#define FLOGGER_H
#include "fb_sh_lib_global.h"

#include <QString>

class FB_SH_LIBSHARED_EXPORT FLogger
{
public:
    /**
     * @brief append Appends message to the log in fileName
     * @param message String that is getting appended
     * @param fileName File where the log is saved
     */
    static void append(QString fileName, QString message);
    /**
     * @brief clear Clears fileName
     * @param fileName File getting cleared
     */
     static void clear(QString fileName);
};

#endif // FLOGGER_H
