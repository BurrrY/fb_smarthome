#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T16:11:19
#
#-------------------------------------------------

QT       -= gui
QT       +=  network sql websockets

TARGET = FB_SH_Lib
TEMPLATE = lib
QMAKE_CXXFLAGS += -std=c++11

OBJECTS_DIR=build
MOC_DIR=build



configJenkins {
    JKWORKSPACE=/var/lib/jenkins/workspace
    JKJOBS=/var/lib/jenkins/jobs

    FBSQL_ROOT=$${JKWORKSPACE}/FB_SQL/
    FBTS_ROOT=$${JKWORKSPACE}/FB_Timespan/
    FBSHLIB_ROOT=$${JKWORKSPACE}/FB_Smarthome/Fb_Smarthome_Solution

    LIBS += -L$${FBSQL_ROOT}/build -lFB_SQL
    INCLUDEPATH += $${FBSQL_ROOT}/FB_SQL
    DEPENDPATH += $${FBSQL_ROOT}/FB_SQL

    LIBS += -L$${FBTS_ROOT}/build -lFB_Timespan
    INCLUDEPATH += $${FBTS_ROOT}/FB_Timespan
    DEPENDPATH += $${FBTS_ROOT}/FB_Timespan
}

DEFINES += FB_SH_LIB_LIBRARY

SOURCES += fb_sh_lib.cpp \
    rule.cpp \
    ruleaction.cpp \
    rulecondition.cpp \
    sh_db.cpp \
    sh_node.cpp \
    sh_node_lm75.cpp \
    sh_node_network.cpp \
    sh_node_OnOff.cpp \
    sh_node_openclose.cpp \
    sh_node_report.cpp \
    sh_node_sensor.cpp \
    sh_node_si7021.cpp \
    sh_node_solar.cpp \
    sh_notifier.cpp \
    flogger.cpp \
    messagebus.cpp \
    webserver.cpp \
    sh_logging.cpp \
    sh_node_gpio.cpp \
    webserver_request.cpp \
    stathelp.cpp \
    sh_node_s0.cpp

HEADERS += fb_sh_lib.h\
        fb_sh_lib_global.h \
    rule.h \
    ruleaction.h \
    rulecondition.h \
    sh_db.h \
    sh_node.h \
    sh_node_lm75.h \
    sh_node_network.h \
    sh_node_num.h \
    sh_node_OnOff.h \
    sh_node_openclose.h \
    sh_node_report.h \
    sh_node_sensor.h \
    sh_node_si7021.h \
    sh_node_solar.h \
    sh_notifier.h \
    dbg.h \
    flogger.h \
    dbschema.h \
    messagebus.h \
    webserver.h \
    sh_eventmessage.h \
    sh_logging.h \
    sh_node_gpio.h \
    webserver_request.h \
    stathelp.h \
    sh_node_s0.h \
    SH_WebReqRes.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}



unix|win32: LIBS += -lFB_SQL
unix|win32: LIBS += -lFB_Timespan

unix|win32: LIBS += -L$$DESTDIR/ -lFB_SQL
unix|win32: LIBS += -L$$DESTDIR/ -lFB_Timespan


INCLUDEPATH += $$PWD/../../../fb_sql/FB_SQL
INCLUDEPATH += $$PWD/../../../fb_timespan/FB_Timespan

DEPENDPATH += $$PWD/../../../fb_sql/FB_SQL
DEPENDPATH += $$PWD/../../../fb_timespan/FB_Timespan



#win32: LIBS += -L$$PWD/../../../OUT/ -lFB_SQL
