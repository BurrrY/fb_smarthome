#ifndef SH_NODE_S0_H
#define SH_NODE_S0_H

#include "fb_sh_lib_global.h"
#include "sh_node_gpio.h"



class FB_SH_LIBSHARED_EXPORT SH_Node_S0 : public SH_Node_GPIO
{
public:
    SH_Node_S0();
    SH_Node_S0(sh_db *db);

signals:

public slots:

    // SH_Node interface
public:
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);
    void checkRules(SH_EventMessage em);
    QString getWebHomeData();

public slots:
    void receiveEvent(SH_EventMessage em);
};

#endif // SH_NODE_S0_H
