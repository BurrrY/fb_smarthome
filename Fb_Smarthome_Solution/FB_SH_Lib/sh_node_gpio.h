#ifndef SH_NODE_GPIO_H
#define SH_NODE_GPIO_H

#include "fb_sh_lib_global.h"
#include "sh_node.h"

#include <QTimer>



class FB_SH_LIBSHARED_EXPORT SH_Node_GPIO : public SH_Node
{
public:
    SH_Node_GPIO();
    SH_Node_GPIO(sh_db *db);

    int getPin() const;
    void setPin(int value);
    bool getInput() const;
    void setInput(bool value);

    // SH_Node interface
    virtual void setJsonConfig(QJsonObject jsonData);
    virtual QStringList getEventList();
    virtual QJsonObject getJsonConfig();
    QString getWebHomeData();
    int getInputCheckInterval() const;
    void setInputCheckInterval(int value);

private slots:
    void timeout();
public slots :
    void receiveEvent(SH_EventMessage em);



private:
    int Pin;
    bool Input;
    bool initDone;
    int inputCheckInterval;
    bool State;

    QTimer pollTimer;

    void initGPIO();

    QString getPinString();
    void setPinValue(bool value);
    bool getPinValue();
};

#endif // SH_NODE_GPIO_H
