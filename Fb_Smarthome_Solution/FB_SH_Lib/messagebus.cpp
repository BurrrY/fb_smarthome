#include "messagebus.h"

MessageBus::MessageBus(int port, bool debug, QObject *parent) :
    QObject(parent),
    pWebSocketServer(new QWebSocketServer(QStringLiteral("Echo Server"),
                                            QWebSocketServer::NonSecureMode, this)),
    clients(),
    debug(debug)
{
    if (pWebSocketServer->listen(QHostAddress::Any, port)) {
        if (debug)
            qDebug() << "MessageBus listening on port" << port;

        connect(pWebSocketServer, &QWebSocketServer::newConnection,
                this, &MessageBus::onNewConnection);

        connect(pWebSocketServer, &QWebSocketServer::closed, this, &MessageBus::closed);
    }
}

MessageBus::~MessageBus()
{
    pWebSocketServer->close();
    qDeleteAll(clients.begin(), clients.end());
}



void MessageBus::onNewConnection()
{
    QWebSocket *pSocket = pWebSocketServer->nextPendingConnection();

    if (debug)
        qDebug() << "socketConnected:" << pSocket->localAddress().toString();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &MessageBus::processTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &MessageBus::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &MessageBus::socketDisconnected);

    clients << pSocket;
}

void MessageBus::processTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    if (debug)
        qDebug() << "Message received:" << message;

    if (pClient)
        pClient->sendTextMessage(message);

}

void MessageBus::processBinaryMessage(QByteArray message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    if (debug)
        qDebug() << "Binary Message received:" << message;

    if (pClient)
        pClient->sendBinaryMessage(message);

}

void MessageBus::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    if (debug)
        qDebug() << "socketDisconnected:" << pClient;

    if (pClient) {
        clients.removeAll(pClient);
        pClient->deleteLater();
    }
}
