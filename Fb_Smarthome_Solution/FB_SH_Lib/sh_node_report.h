#ifndef SH_NODE_REPORT_H
#define SH_NODE_REPORT_H
#include "fb_sh_lib_global.h"

#include "sh_node.h"



class FB_SH_LIBSHARED_EXPORT  SH_Node_Report : public SH_Node
{
public:
    SH_Node_Report(sh_db *db);

    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);

};

#endif // SH_NODE_REPORT_H
