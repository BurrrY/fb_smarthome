#include "sh_notifier.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

SH_Notifier::SH_Notifier()
{
    qCDebug(shLibNotifier, "Creating SH_Notifier object");
  //  db = sh_db::getInstance();
    //pbToken = db->loadSetting(CFG_PB_Token).toString();
    connect(&nwManager, &QNetworkAccessManager::finished, this, &SH_Notifier::nwManagerFinished);

}

void SH_Notifier::msg(QString msg, int ID, int level)
{
    qCDebug(shLibNotifier) << "Got Message: " << msg << " ID: " << ID << " Level: " << level;
    pbMessage("FROM: " + QString::number(ID), msg);
   // db->insertAlert(ID, level, msg);
}

void SH_Notifier::pbMessage(QString title, QString msg) {
   // QUrl pbUrl("http://localhost:8080/v2/pushes");
    QUrl pbUrl("https://api.pushbullet.com/v2/pushes");
  //  pbUrl.setHost("https://api.pushbullet.com");
  //  pbUrl.setHost
  //  pbUrl.setPath("/v2/pushes");


    QJsonObject json;
    json["body"]  = msg;
    json["title"]  = title;
 //   json["client_iden"] = "FB_Smarthome";
    json["type"]  = "note";

    QJsonDocument doc(json);
    QString Data = doc.toJson(QJsonDocument::Compact);


    pbRequest = QNetworkRequest(pbUrl);
    pbRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    pbRequest.setRawHeader("Access-Token", "o.P5gdi8rWxpnr8QV3kncgZbOGZ1YGMIBF");

    qCDebug(shLibNotifier) << "FROM: " << pbUrl.toString();
    qCDebug(shLibNotifier) << "POST: " << Data.toLatin1();
    nwManager.post(pbRequest, Data.toLatin1());

}


void SH_Notifier::nwManagerFinished(QNetworkReply *reply)
{
    qCDebug(shLibNotifier) << "nwManagerFinished: ";
    QByteArray responseData;
    reply->waitForReadyRead(1000);
    while(reply->bytesAvailable()) {
       responseData.append( reply->readAll());

    }


    qCDebug(shLibNotifier) << "Response: " << responseData;

}
