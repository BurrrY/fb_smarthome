#include "sh_node_solar.h"
#include "stathelp.h"

SH_Node_Solar::SH_Node_Solar(sh_db *db) :
    SH_Node_Network(db)
{
    GroupType = Network;
	
    requestRunning = false;
    pollTimer.start(1000 * 60 * updateInterval);
    timeoutCounter.setSingleShot(true);


    connect(&nwManager, &QNetworkAccessManager::finished, this, &SH_Node_Solar::gotSolarResponse);
    connect(&pollTimer, &QTimer::timeout, this, &SH_Node_Solar::getData);
    connect(&timeoutCounter, &QTimer::timeout, this, &SH_Node_Solar::gotTimeout);
}


void SH_Node_Solar::getData() {

    if(requestRunning) {
        qCDebug(shLibNodeSolar) << "Request already running on " << IP.toString();
        return;
    }


    if(IP.toIPv4Address()<=0){
        qWarning() << "Invalid IP ("+IP.toString()+")on Node " +getName() ;
        pollTimer.stop();
        return;
    }

    requestRunning = true;

    QUrl target;
    QString host = "http://" + IP.toString();
    target.setUrl(host);
    target.setPassword("pvwr");
    target.setUserName("pvserver");

    fetchData(target);
    timeoutCounter.start(1000 * 30);
}


void SH_Node_Solar::gotTimeout()
{
    if(requestRunning) {
        qCWarning(shLibNodeSolar) << "Timeout from " << IP.toString();
        requestRunning = false;
    }
}

int SH_Node_Solar::getRecentSum(int days, int offset)
{
    QDateTime start =QDateTime::currentDateTime(); //QDateTime::fromString("2016-03-16 21:12:52", "yyyy-MM-dd hh:mm:ss");

    start = start.addDays(offset * -1);
    QDateTime end = start.addDays(days * -1);
    quint64 powerSum = 0;

    QString query = QString("SELECT Timestamp, W_Total as power FROM solarlog WHERE ") +
            "timestamp < \"" + sh_db::SQL_DATEFORMAT(start) + "\" and timestamp > \"" + sh_db::SQL_DATEFORMAT(end) +
            "\" AND Source = " + QString::number(this->ID) + " ORDER BY timestamp asc";

 //   QString query = QString("SELECT Timestamp, W_Total as power, Source FROM solarlog  WHERE timestamp < \"2016-03-16 21:12:52\" and timestamp > \"2016-03-15 21:12:52\" AND Source = 13 ORDER BY timestamp asc");

    DB_DataSet d = db->getData(query);

    if(d.count()==0) //no Data
        return 0;

    QDateTime t, t_last;
    t_last = d[0]["Timestamp"].toDateTime();

    for(int i=0;i<d.count()-1;++i) {
        DB_DataRow row = d[i];
        t = row["Timestamp"].toDateTime();

        qint64 durationSec = t_last.secsTo(t);
        int val = row["power"].toInt();

        powerSum += (durationSec * val);

        t_last = t;
    }

    powerSum /= 60; //to minutes
    powerSum /= 60; //to hours
    return (powerSum);
}

int SH_Node_Solar::getUpdateInterval() const
{
    return updateInterval;
}

void SH_Node_Solar::setUpdateInterval(int value)
{
    if(value > 9999 || value < 0)
        updateInterval = 5;
    else
        updateInterval = value;
}

QString SH_Node_Solar::getWebHomeData()
{
    int today = getRecentSum(1,0);
  //  int yesterday = getRecentSum(1,1);
    int week = getRecentSum(7,0);
    return "<a href=\"/solar.html?days=30\"<span class=\"categoryNodeTitle\">"+this->Name+":</span></a><br>In 24h: "+
            QString::number((float)today/1000, 'g', 2 )+"<span class=\"small\">kWh</span>"+
            "<br>In 7 Tagen: "+QString::number((float)week/1000, 'g', 2)+"<span class=\"small\">kWh</span>";
}

void SH_Node_Solar::gotSolarResponse(QNetworkReply *reply)
{
    QString src = reply->readAll();

    const QString aIdent = "aktuell</td>\r\n<td width=\"70\" align=\"right\" bgcolor=\"#FFFFFF\">";
    const QString sIdent = "Status</td>\r\n<td colspan=\"4\">\r\n";

    int a = src.indexOf(aIdent);
    int s = src.indexOf(sIdent);



    if(a == -1 || s == -1) {
        qCWarning(shLibNodeSolar) << "Could not read response from  " << IP.toString();
        requestRunning = false;
        qCWarning(shLibNodeSolar) << reply->errorString();
        return;
    }

    //apply offset
    a += aIdent.length();
    s += sIdent.length();

    int a_end = src.indexOf("</td>", a);
    int s_end = src.indexOf("</td>", s);

    QString pwr_s = src.mid(a, a_end - a).trimmed();
    QString state = src.mid(s, s_end - s).trimmed();

    qCDebug(shLibNodeSolar) << "SolarData from "  << IP.toString() << ": " << pwr_s << ", " << state;

    bool ok;
    int pwr = pwr_s.toInt(&ok);

    if(!ok && state == "Aus") {
        ok = true;
        pwr = 0;
    }

    if(ok) {
        db->insertSolarData(pwr, ID);
  //      checkRules(em);
    }
    else {
        qCDebug(shLibNodeSolar) << "Could not insert Data from " << IP.toString();
    }

    requestRunning = false;

}

QJsonObject SH_Node_Solar::getJsonConfig()
{
    QJsonObject json(SH_Node_Network::getJsonConfig());

    json.insert("nodeUpdateInterval", updateInterval);


    QJsonDocument doc(json);

    return json; //.toJson(QJsonDocument::Compact);
}

void SH_Node_Solar::setJsonConfig(QJsonObject jsonData)
{
    SH_Node_Network::setJsonConfig(jsonData);
    int val;

    if(jsonData.contains("nodeUpdateInterval"))
    {
        val = StatHelp::JsonToInt(jsonData.value("nodeUpdateInterval"));


        if(val != -999)
            setUpdateInterval(val);
        else
            qCWarning(shLibNodeSolar) << "Cannot save new Updateinterval on node " << ID;
    }
}
