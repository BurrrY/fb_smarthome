#include "sh_node_report.h"

SH_Node_Report::SH_Node_Report(sh_db *db) :
    SH_Node(db)
{

}


QJsonObject SH_Node_Report::getJsonConfig()
{
    QJsonObject json(SH_Node::getJsonConfig());

    QJsonDocument doc(json);
    return json; //.toJson(QJsonDocument::Compact);
}

void SH_Node_Report::setJsonConfig(QJsonObject jsonData)
{
    SH_Node::setJsonConfig(jsonData);

}
