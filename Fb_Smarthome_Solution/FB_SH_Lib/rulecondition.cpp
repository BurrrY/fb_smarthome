#include "rulecondition.h"

RuleCondition::RuleCondition() {
    qCInfo(shRulesConditions) << ("Calling RuleCondition()");
    Comparator = "=";
    Data = "";
    sourceNodeID = -1;
}


bool RuleCondition::isValid(SH_EventMessage em) {
    qCInfo(shRulesConditions, "Calling isValid(em)");
	
	if(Comparator == "") {
		compareResult = "Comparator empty.";
        qCDebug(shRulesConditions) << ("Tried to run isValid without Comparator!");
		return false;
	}
	
	if(sourceNodeID == -1) {
		compareResult = "SourceNodeID invalid.";
        qCDebug(shRulesConditions) << ("Tried to run isValid with invalid SourceNodeID!");
		return false;
	}
	
	
	if(em.sender==sourceNodeID) {
		if(Comparator == "=") {
			if(em.parameter.value("data") == Data) {
				compareResult = "Condition valid";
				return true;
			}
			else {
				compareResult = "Data mismatch.";
				return false;
			}
		}
		else {
			compareResult = "Comparator invalid: " + Comparator;
            qCDebug(shRulesConditions) << ("Tried to run isValid with invalid Comparator: " + Comparator);
			return false;
		}
	}
	else {
		compareResult = "Sender/Source mismatch.";
		return false;
	}		
}


QString RuleCondition::getCompareResult(){
	return compareResult;
}


QString RuleCondition::getComparator(){
	return Comparator;
}


void RuleCondition::setComparator(const QString &cmp){
	if(cmp == "=" || cmp == ">" || cmp == "<")
		Comparator = cmp;
	else {
		Comparator = "";		
        qCDebug(shRulesConditions) << ("Tried to set invalid Comparator: " + cmp);
	}
}
