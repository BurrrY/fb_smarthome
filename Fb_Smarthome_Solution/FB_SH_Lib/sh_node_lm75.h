#ifndef SH_NODE_TEMPERATURE_H
#define SH_NODE_TEMPERATURE_H
#include "fb_sh_lib_global.h"

#include "sh_node_sensor.h"

#include <QTimer>



class FB_SH_LIBSHARED_EXPORT  SH_Node_LM75 : public SH_Node_Sensor
{
public:
    SH_Node_LM75(sh_db *db);

    QString getLM75Address() const;
    void setLM75Address(const QString &value);

    int getUpdateIntervalSec() const;
    void setUpdateIntervalSec(int value);

    int getLatestValue() const;

    QDateTime getLatestMeasurement() const;

    // Virtual Methods
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);


private slots:
    void getData();
    void gotTempResponse(QNetworkReply *reply);
private:
    QTimer pollTimer;
    QString LM75Address;
    int updateIntervalSec;
    bool requestRunning;
    int latestValue;
    QDateTime latestMeasurement;

    // SH_Node_Sensor interface
public:
    QString getIcon();
    QString getText();
    QString getSubText();
};

#endif // SH_NODE_TEMPERATURE_H
