#ifndef FB_SH_LIB_GLOBAL_H
#define FB_SH_LIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(FB_SH_LIB_LIBRARY)
#  define FB_SH_LIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define FB_SH_LIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // FB_SH_LIB_GLOBAL_H
