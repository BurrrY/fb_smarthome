#include "sh_node_lm75.h"
#include "stathelp.h"

SH_Node_LM75::SH_Node_LM75(sh_db *db) :
    SH_Node_Sensor(db)
{
    GroupType = Network;

    LM75Address = "-";
    updateIntervalSec = 60;

    requestRunning = false;
    connect(&nwManager, &QNetworkAccessManager::finished, this, &SH_Node_LM75::gotTempResponse);
    connect(&pollTimer, &QTimer::timeout, this, &SH_Node_LM75::getData);

}

QString SH_Node_LM75::getLM75Address() const
{
    return LM75Address;
}

void SH_Node_LM75::setLM75Address(const QString &value)
{
    LM75Address = value;
}

int SH_Node_LM75::getUpdateIntervalSec() const
{
    return updateIntervalSec;
}

void SH_Node_LM75::setUpdateIntervalSec(int value)
{
    updateIntervalSec = value;
}


QJsonObject SH_Node_LM75::getJsonConfig()
{
    qDebug () << "SH_Node_Temperature::getJsonConfig()";

    QJsonObject json(SH_Node_Network::getJsonConfig());
    json.insert("UpdateIntervalSec", updateIntervalSec);
    json.insert("LM75Address", LM75Address);

    //QJsonDocument doc(json);
    return json; //c.toJson(QJsonDocument::Indented);
}

void SH_Node_LM75::setJsonConfig(QJsonObject jsonData)
{
    SH_Node_Network::setJsonConfig(jsonData);
    updateIntervalSec = StatHelp::JsonToInt(jsonData["UpdateIntervalSec"]);

    if(updateIntervalSec<60)
        updateIntervalSec = 60;

    LM75Address = StatHelp::JsonToQString(jsonData["LM75Address"]);

    if(LM75Address.length() <= 0)
        LM75Address = "-";

    if(updateIntervalSec >= 60 && IP.toIPv4Address() > 0) {
        pollTimer.start(1000 * updateIntervalSec);
        getData();
    }
}


void SH_Node_LM75::getData() {

    if(requestRunning) {
        qDebug() << "Request already running on " << IP.toString();
        return;
    }

    if(IP.toIPv4Address()<=0){
        qWarning() << "Invalid IP ("+IP.toString()+")on Node " +getName() ;
        pollTimer.stop();
        return;
    }

    requestRunning = true;

    QUrl target;
    target.setUrl("http://" + IP.toString());
    target.setPath("/temp/" + LM75Address);
    qDebug() << "get Data from " << target.toString();

    fetchData(target);
}


void SH_Node_LM75::gotTempResponse(QNetworkReply *reply)
{
    QJsonDocument jDoc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject jObj = jDoc.object();

    if(jObj.count() == 3) {
        latestValue = jObj["temp"].toInt();
        qDebug() << "Got Temp-Data from " << IP.toString() << ", Temp: " << latestValue;


        latestMeasurement = QDateTime::currentDateTime();
        db->saveTemp(latestValue, ID);

    }
    else if(jObj.count() == 1) {
        qDebug() << "Error reading from " << IP.toString() << ", Temp: " << jObj["err"].toString();
    }
    else {
        qDebug() << "Unkown error reading from " << IP.toString();
    }

    requestRunning = false;

}

QDateTime SH_Node_LM75::getLatestMeasurement() const
{
    return latestMeasurement;
}

int SH_Node_LM75::getLatestValue() const
{
    return latestValue;
}


QString SH_Node_LM75::getIcon()
{
    if(latestValue/100>35)
        return "temp_hot";
    else if(latestValue/100<15)
        return "temp_low";
    else
        return "temp_mid";
}

QString SH_Node_LM75::getText()
{
    return "Temperature: " + QString::number(latestValue) + "°C";
}

QString SH_Node_LM75::getSubText()
{
    return "Measured on: " + latestMeasurement.toString("hh:mm");
}
