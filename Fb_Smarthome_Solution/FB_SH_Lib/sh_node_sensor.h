#ifndef SH_NODE_SENSOR_H
#define SH_NODE_SENSOR_H
#include "fb_sh_lib_global.h"

#include "sh_node_network.h"



class FB_SH_LIBSHARED_EXPORT  SH_Node_Sensor : public SH_Node_Network
{
public:
    SH_Node_Sensor();
    SH_Node_Sensor(sh_db *db);

    virtual QString getIcon() = 0;
    virtual QString getText() = 0;
    virtual QString getSubText() = 0;
};

#endif // SH_NODE_SENSOR_H
