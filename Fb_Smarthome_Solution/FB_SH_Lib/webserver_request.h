#ifndef WEBSERVER_REQUEST_H
#define WEBSERVER_REQUEST_H

#include <QMap>
#include <QString>
#include <QTcpSocket>



class Webserver_Request
{
public:
    Webserver_Request();
    Webserver_Request(QTcpSocket *socket);

    void setMethod(const QString &value);

    void setRequestString(const QString &value);

    void serve();

private:
    QTcpSocket *socket;
    QString method;
    QString requestString;
    QMap<QString, QString> requestData;

};

#endif // WEBSERVER_REQUEST_H
