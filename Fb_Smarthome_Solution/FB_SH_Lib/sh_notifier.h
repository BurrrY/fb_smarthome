#ifndef SH_NOTIFIER_H
#define SH_NOTIFIER_H
#include "fb_sh_lib_global.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QString>

#include "sh_logging.h"
//#include "sh_db.h"
#include "sh_node_num.h"




class FB_SH_LIBSHARED_EXPORT  SH_Notifier : public  QObject
{
    Q_OBJECT

public:
    SH_Notifier();
    void msg(QString msg, int ID, int level);
private slots:
    void nwManagerFinished(QNetworkReply *reply);
private:
  //  sh_db *db;
    QString pbToken;
    void pbMessage(QString title, QString msg);
    QNetworkRequest pbRequest;
    QNetworkAccessManager nwManager;
};

#endif // SH_NOTIFIER_H
