#include "sh_logging.h"

Q_LOGGING_CATEGORY(shLibNotifier, "sh.Lib.Notifier")
Q_LOGGING_CATEGORY(shLibDB, "sh.Lib.DB")

Q_LOGGING_CATEGORY(shLibNode, "sh.Lib.Node")
Q_LOGGING_CATEGORY(shLibNodeSolar, "sh.Lib.Node.Solar")


Q_LOGGING_CATEGORY(shLibWeb, "sh.Lib.Web")
Q_LOGGING_CATEGORY(shLibWebConfig, "sh.Lib.Web.config")


Q_LOGGING_CATEGORY(shRules, "sh.Rules")
Q_LOGGING_CATEGORY(shRulesConditions, "sh.Rules.Conditions")
Q_LOGGING_CATEGORY(shRulesActions, "sh.Rules.Actions")
