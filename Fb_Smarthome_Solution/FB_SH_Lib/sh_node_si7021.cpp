#include "sh_node_si7021.h"


SH_Node_SI7021::SH_Node_SI7021(sh_db *db) :
    SH_Node_Sensor(db)
{
    GroupType = Network;

    updateIntervalSec = 60;

    requestRunning = false;
    connect(&nwManager, &QNetworkAccessManager::finished, this, &SH_Node_SI7021::gotTempResponse);
    connect(&pollTimer, &QTimer::timeout, this, &SH_Node_SI7021::getData);
}

int SH_Node_SI7021::getUpdateIntervalSec() const
{
    return updateIntervalSec;
}

void SH_Node_SI7021::setUpdateIntervalSec(int value)
{
    updateIntervalSec = value;
}


QJsonObject SH_Node_SI7021::getJsonConfig()
{
    qDebug () << "SH_Node_SI7021::getJsonConfig()";

    QJsonObject json(SH_Node_Network::getJsonConfig());
    json["UpdateIntervalSec"]  = updateIntervalSec;

    QJsonDocument doc(json);
    return json; //.toJson(QJsonDocument::Indented);
}

void SH_Node_SI7021::setJsonConfig(QJsonObject jsonData)
{
    SH_Node_Network::setJsonConfig(jsonData);
    updateIntervalSec = jsonData["UpdateIntervalSec"].toInt();

    if(updateIntervalSec<60)
        updateIntervalSec = 60;

    if(updateIntervalSec >= 60) {
        pollTimer.start(1000 * updateIntervalSec);
        getData();
    }
}


void SH_Node_SI7021::getData() {

    if(requestRunning) {
        qDebug() << "Request already running on " << IP.toString();
        return;
    }

    if(IP.toIPv4Address()<=0){
        qWarning() << "Invalid IP ("+IP.toString()+")on Node " +getName() ;
        pollTimer.stop();
        return;
    }

    requestRunning = true;

    QUrl target;
    target.setUrl("http://" + IP.toString());
    target.setPath("/si7021");
    qDebug() << "get Data from " << target.toString();

    fetchData(target);
}


void SH_Node_SI7021::gotTempResponse(QNetworkReply *reply)
{
    QJsonDocument jDoc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject jObj = jDoc.object();

    if(jObj.count() == 3) {
        latestTemp = jObj["temp"].toInt();
        latestHumi = jObj["humi"].toInt();

        qDebug() << "Got Data from " << IP.toString() << ", Temp: " << latestTemp << ", Humi: " << latestHumi;


        latestMeasurement = QDateTime::currentDateTime();
        db->saveSI7021(latestTemp, latestHumi, ID);
        //checkRules(em);
    }
    else if(jObj.count() == 1) {
        qDebug() << "Error reading from " << IP.toString() << ", Temp: " << jObj["err"].toString();
    }
    else {
        qDebug() << "Unkown error reading from " << IP.toString();
    }

    requestRunning = false;

}

float SH_Node_SI7021::getLatestHumi() const
{
    return latestHumi/100;
}

float SH_Node_SI7021::getLatestTemp() const
{
    return latestTemp/100;
}

QDateTime SH_Node_SI7021::getLatestMeasurement() const
{
    return latestMeasurement;
}


QString SH_Node_SI7021::getIcon()
{
    if(latestTemp/100>35)
        return "temp_hot";
    else if(latestTemp/100<15)
        return "temp_low";
    else
        return "temp_mid";
}

QString SH_Node_SI7021::getText()
{
    return "Temperature: " + QString::number(latestTemp/100) + "°C, " + "Humidity:" + QString::number(latestHumi/100) + "%";
}

QString SH_Node_SI7021::getSubText()
{
    return "Measured on: " + latestMeasurement.toString("hh:mm");
}
