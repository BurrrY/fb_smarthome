#ifndef SH_NODE_OPENCLOSE_H
#define SH_NODE_OPENCLOSE_H
#include "fb_sh_lib_global.h"

#include "sh_node_network.h"



class FB_SH_LIBSHARED_EXPORT  SH_Node_OpenClose : public SH_Node_Network
{
public:
    SH_Node_OpenClose(sh_db *db);


    // Virtual Methods
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);
	
public slots:
    virtual void receiveEvent(SH_EventMessage em);
};
#endif // SH_NODE_OPENCLOSE_H
