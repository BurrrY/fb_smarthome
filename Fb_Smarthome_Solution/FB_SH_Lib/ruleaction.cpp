#include "ruleaction.h"

RuleAction::RuleAction()
{

}

RuleAction::RuleAction(QString RuleData, RuleActionType RuleType)
{
    Data = RuleData;
    Type = RuleType;
}

bool RuleAction::exec(int nodeID)
{
    SH_Notifier n;

    if(Type == RuleActionType::sendAlert || Type == RuleActionType::sendWarning)
        n.msg(Data, nodeID, (int)Type);

    return true;
}
