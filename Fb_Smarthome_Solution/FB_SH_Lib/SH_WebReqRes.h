#ifndef SHREQUESTRESPONSE
#define SHREQUESTRESPONSE

#include "fb_sh_lib_global.h"

#include <QDateTime>

class FB_SH_LIBSHARED_EXPORT SH_WebReqRes {

public:
    SH_WebReqRes() { }
    SH_WebReqRes(QString requested, QString err, QString target) {
		Error = err;
		RequestOn = requested;
		RedirecTo = target;
    }

    SH_WebReqRes(QString requested, QString err, QString target, QString desc)  {
		Error = err;
		RequestOn = requested;
		RedirecTo = target;
		Description = desc;
    }

	QString Error;
    QString Description;
	QString RedirecTo;
	QString RequestOn;
	
	QString ToJson() {
		return "";
	}
};

#endif // SHREQUESTRESPONSE
