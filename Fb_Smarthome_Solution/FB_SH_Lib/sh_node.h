#ifndef SH_NODE_H
#define SH_NODE_H
#include "fb_sh_lib_global.h"

#include "rule.h"
#include "sh_db.h"
#include "sh_eventmessage.h"

#include <QDateTime>
#include <QMap>
#include <QObject>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include "sh_node_num.h"


class FB_SH_LIBSHARED_EXPORT  SH_Node : public QObject
{
    Q_OBJECT


public:
    SH_Node();
    SH_Node(sh_db *db);
    ~SH_Node();


    QString getName() const;
    void setName(const QString &value);

    int getID() const;
    void setID(int value);


	//Type
    QString getType() const;
    void setTypeID(int value);


    // STATICS
    static int TypeToID(QString type);
    static int TypeID(QString type);
    static QString TypeToString(int typeID);
    static QStringList *NodeTypes;
    static QString StateToString(NodeState state);
    static SH_Node* nodeFactory(int nodeType, sh_db *db);

	//DB-Related
    sh_db *getDB() const;
    bool saveNode();
    void setBasicInfo(DB_DataRow r);

    virtual QJsonObject getJsonConfig();
    virtual void setJsonConfig(QJsonObject jsonData);
	
    virtual QStringList getEventList();
	
    int getTypeID() const;
    QString getTypeName() const;

    void addRule(Rule *r);
    virtual void checkRules(SH_EventMessage em);
    QList<Rule *> getRules() const;

    NodeGroup getGroupType() const;

    bool getShowOnHome() const;
    void setShowOnHome(bool value);
    QString getHomeCategory() const;
    void setHomeCategory(QString value);
    virtual QString getWebHomeData();
	
signals:
	void changed(SH_EventMessage em);
	
public slots:
    virtual void receiveEvent(SH_EventMessage em);

protected:
    QString Name;
    int ID;
    int typeID;
    bool showOnHome;
    QString homeCategory;

    sh_db *db;

    QList<Rule *> rules;
    NodeGroup GroupType;

};


#endif // SH_NODE_H
