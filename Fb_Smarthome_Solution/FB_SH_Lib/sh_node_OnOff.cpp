#include "sh_node_OnOff.h"



SH_Node_OnOff::SH_Node_OnOff(sh_db *db) :
    SH_Node_Network(db)
{
    GroupType = Network;

}



SH_Node_OnOff::~SH_Node_OnOff()
{

}


void SH_Node_OnOff::receiveEvent(SH_EventMessage em)
{
	qWarning() << "SH_Node_OpenClose::receiveEvent not implemented! ";
	/*
QUrl target;
    QString host = "http://" + IP.toString();
    target.setUrl(host);
    target.setPort(Port);

    if(action=="on")
    {
        this->State = On;
        target.setPath(NODE_CMD_ON);
        fetchData(target);
        qDebug() << target.toString();
        db->insertSwitchHistory(ID, (int)State);
        switchHistory.insert(QDateTime::currentDateTime(), SwitchHistory(QDateTime::currentDateTime(), getState()));
        //checkRules(em);
    }
    else if(action=="off")
    {
        this->State = Off;
        target.setPath(NODE_CMD_OFF);
        fetchData(target);
        qDebug() << target.toString();
        db->insertSwitchHistory(ID, (int)State);
        switchHistory.insert(QDateTime::currentDateTime(), SwitchHistory(QDateTime::currentDateTime(), getState()));
       // checkRules(em);
    }

    return true;
	*/
}

QJsonObject SH_Node_OnOff::getJsonConfig()
{
    QJsonObject json(SH_Node_Network::getJsonConfig());

    QJsonDocument doc(json);
    return json; //.toJson(QJsonDocument::Compact);
}

void SH_Node_OnOff::setJsonConfig(QJsonObject jsonData)
{
    SH_Node_Network::setJsonConfig(jsonData);

}
