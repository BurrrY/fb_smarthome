#ifndef SHEVENTMESSAGE
#define SHEVENTMESSAGE

#include "fb_sh_lib_global.h"

#include <QDateTime>
#include <QMap>

enum EventSourceComponent { ESC_Webserver, ESC_GPIO, ESC_Notify, ESC_None };

class FB_SH_LIBSHARED_EXPORT SH_EventMessage {

public:
    SH_EventMessage() {
		sender = -1;
		timestamp = QDateTime::currentDateTime();
		source = ESC_None;
	}
	
    SH_EventMessage(int Sender, QString Data){        
		sender = Sender<=0?-1:Sender; // -1 if ID < 0		
        parameter.insert("data", Data);
		timestamp = QDateTime::currentDateTime();
		source = ESC_None;
	}
	
    SH_EventMessage(int Sender, QString Data, EventSourceComponent src){        
		sender = Sender<=0?-1:Sender; // -1 if ID < 0		
        parameter.insert("data", Data);
		timestamp = QDateTime::currentDateTime();
		source = src;
    }
	
    QMap<QString, QString> parameter;
    QDateTime timestamp;
    int sender;
    EventSourceComponent source;



};

#endif // SHEVENTMESSAGE
