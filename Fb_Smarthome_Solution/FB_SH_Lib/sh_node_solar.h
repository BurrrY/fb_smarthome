#ifndef SH_NODE_SOLAR_H
#define SH_NODE_SOLAR_H
#include "fb_sh_lib_global.h"

#include "sh_node_network.h"

#include <QTimer>



class FB_SH_LIBSHARED_EXPORT  SH_Node_Solar : public SH_Node_Network
{
public:
    SH_Node_Solar(sh_db *db);


    // Virtual Methods
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);

    int getUpdateInterval() const;
    void setUpdateInterval(int value);
    QString getWebHomeData();
private slots:
    void getData();
    void gotSolarResponse(QNetworkReply *reply);
    void gotTimeout();

private:
    int getRecentSum(int days, int offset);
    QTimer pollTimer;
    QTimer timeoutCounter;
    int updateInterval;
    bool requestRunning;
};

#endif // SH_NODE_SOLAR_H
