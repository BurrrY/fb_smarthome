#ifndef DBSCHEMA_H
#define DBSCHEMA_H


#include <fb_sql.h>


QMap<QString, QList<TableField>>     DBSchema;

QList<TableField> fieldSet;
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("NodeID", "INTEGER", true);
fieldSet << TableField("DateTime", "Datetime", true);
fieldSet << TableField("Level", "INTEGER", true);
fieldSet << TableField("Message", "TEXT", true);
DBSchema.insert("Alerts", fieldSet);


fieldSet.clear();
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("Name", "INTEGER", true);
fieldSet << TableField("IP", "TEXT");
fieldSet << TableField("Port", "INTEGER");
fieldSet << TableField("Type", "INTEGER");
fieldSet << TableField("LatestState", "INTEGER");
fieldSet << TableField("Script", "TEXT");
fieldSet << TableField("JsonConfig", "TEXT");
DBSchema.insert("Nodes", fieldSet);


fieldSet.clear();
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("Timestamp", "DATETIME");
fieldSet << TableField("W_Total", "NUMERIC");
DBSchema.insert("SolarLog", fieldSet);



fieldSet.clear();
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("timestamp", "DATETIME");
fieldSet << TableField("NodeID", "NUMERIC", true);
fieldSet << TableField("State", "NUMERIC", true);
DBSchema.insert("SwitchHistory", fieldSet);


fieldSet.clear();
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("Timestamp", "DATETIME");
fieldSet << TableField("NodeID", "NUMERIC", true);
fieldSet << TableField("Value", "NUMERIC", true);
DBSchema.insert("TempLog", fieldSet);


fieldSet.clear();
fieldSet << TableField("ID", "INTEGER", true, true, true, true);
fieldSet << TableField("NodeID", "NUMERIC", true);
fieldSet << TableField("TempValue", "NUMERIC", true);
fieldSet << TableField("HumiValue", "NUMERIC", true);
fieldSet << TableField("Timestamp", "DATETIME");
DBSchema.insert("SI7021Log", fieldSet);


DB_FieldList Settings;
Settings << TableField("ID", "INTEGER", true, true, true, true);
Settings << TableField("Key", "TEXT", true);
Settings << TableField("Value", "TEXT", true);
DBSchema.insert("Settings", Settings);



DB_FieldList Rules;
Rules << TableField("ID", "INTEGER", true, true, true, true);
Rules << TableField("SourceNode", "NUMERIC", true);
Rules << TableField("TargetNode", "NUMERIC", true);
Rules << TableField("Comparator", "TEXT", true);
Rules << TableField("Data", "TEXT", true);
Rules << TableField("Reaction", "TEXT", true);
Rules << TableField("Delay", "NUMERIC", true);
DBSchema.insert("Rules", Rules);


#endif // DBSCHEMA_H
