#ifndef RULEACTION_H
#define RULEACTION_H
#include "fb_sh_lib_global.h"

#include <QString>
#include "sh_notifier.h"


enum RuleActionType {
    sendWarning = 10,
    sendAlert = 20
};


class FB_SH_LIBSHARED_EXPORT  RuleAction
{
public:
    RuleAction();
    RuleAction(QString RuleData, RuleActionType RuleType);
    RuleActionType Type;
    QString Data;
    bool exec(int nodeID);

};

#endif // RULEACTION_H
