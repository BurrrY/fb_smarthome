#include "sh_node_gpio.h"
#include "stathelp.h"


SH_Node_GPIO::SH_Node_GPIO()
{
    Pin = -1;
    Input = true;
    initDone = false;
    inputCheckInterval=60;
    connect(&pollTimer, &QTimer::timeout, this, &SH_Node_GPIO::timeout);
}

SH_Node_GPIO::SH_Node_GPIO(sh_db *db):
    SH_Node(db)
{
    Pin = -1;
    Input = true;
    initDone = false;
    inputCheckInterval=60;
    connect(&pollTimer, &QTimer::timeout, this, &SH_Node_GPIO::timeout);
}


void SH_Node_GPIO::setJsonConfig(QJsonObject jsonData)
{
    SH_Node::setJsonConfig(jsonData);

    if(jsonData.contains("PinNo"))
        setPin(StatHelp::JsonToInt(jsonData.value("PinNo")));

    if(jsonData.contains("PinCheckInterval"))
        setInputCheckInterval(StatHelp::JsonToInt(jsonData.value("PinCheckInterval")));

    if(jsonData.contains("PinInput")) {
        setInput(StatHelp::JsonToBool(jsonData.value("PinInput")));
        if(Input) {
            if(inputCheckInterval<5)
                inputCheckInterval = 5;

            pollTimer.start(1000 * inputCheckInterval);
        }
    }
}



QJsonObject SH_Node_GPIO::getJsonConfig()
{
    QJsonObject json(SH_Node::getJsonConfig());
    json.insert("PinNo", getPin());
    json.insert("PinInput", getInput());
    json.insert("PinCheckInterval", inputCheckInterval);

    return json;
}


 QStringList SH_Node_GPIO::getEventList() {
   QStringList events(SH_Node::getEventList());
   events << "on" << "off";
   return events;
}

int SH_Node_GPIO::getPin() const
{
    return Pin;
}

void SH_Node_GPIO::setPin(int value)
{
    Pin = value;
}

bool SH_Node_GPIO::getInput() const
{
    return Input;
}

void SH_Node_GPIO::setInput(bool value)
{
    Input = value;
}

void SH_Node_GPIO::timeout()
{
    getPinValue();
}

int SH_Node_GPIO::getInputCheckInterval() const
{
    return inputCheckInterval;
}

void SH_Node_GPIO::setInputCheckInterval(int value) {
	if(value<5)
		inputCheckInterval = 5;
	else
		inputCheckInterval = value;
}

void SH_Node_GPIO::initGPIO()
{
    // echo "17" > /sys/class/gpio/export
	#ifdef __linux__
        QFile fExport("/sys/class/gpio/export");
    #elif _WIN32
        QFile fExport("./IOSim/export.txt");
    #endif


    if ( fExport.open(QIODevice::WriteOnly) )
    {
        fExport.write((QString::number(Pin)+"\n").toLatin1());
        fExport.close();
    }

    //echo "out" > /sys/class/gpio/gpio17/direction
    #ifdef __linux__
        QFile fDirection("/sys/class/gpio/"+getPinString()+"/direction");
    #elif _WIN32
        QFile fDirection("./IOSim/"+getPinString()+"/direction.txt");
    #endif

    if ( fDirection.open(QIODevice::WriteOnly) )
    {
        if(Input)
            fDirection.write("in\n");
        else
            fDirection.write("out\n");

        fDirection.close();
    }

    initDone = true;
}


void SH_Node_GPIO::setPinValue(bool value) {
	
    //echo "1" > /sys/class/gpio/gpio17/value
    if(!initDone)
        initGPIO();

    State = value;

    #ifdef __linux__
        QFile fDirection("/sys/class/gpio/"+getPinString()+"/value");
    #elif _WIN32
        QFile fDirection("./IOSim/"+getPinString()+"/value.txt");
    #endif

    if ( fDirection.open(QIODevice::WriteOnly) )
    {
        if(value)
            fDirection.write("1\n");
        else
            fDirection.write("0\n");

        fDirection.close();
    }
}



bool SH_Node_GPIO::getPinValue() {
	
    //echo "1" > /sys/class/gpio/gpio17/value
    bool oldState = State;

    if(!initDone)
        initGPIO();

    #ifdef __linux__
        QFile fDirection("/sys/class/gpio/"+getPinString()+"/value");
    #elif _WIN32
        QFile fDirection("./IOSim/"+getPinString()+"/value.txt");
    #endif

    if ( fDirection.open(QIODevice::ReadOnly) )
    {
        QString val = fDirection.readAll();
        if(val.contains("1")) //sometimes there is a \n
            State = true;
        else
            State = false;
    }

    if(oldState != State)
    {
        emit changed(SH_EventMessage(ID, State?"on":"off"));
    }


    return State;
}

QString SH_Node_GPIO::getPinString() {
    return "gpio" + QString::number(Pin);
}


void SH_Node_GPIO::receiveEvent(SH_EventMessage em)
{
	if(em.sender == ID) {
		if(em.parameter.contains("data")){
			if(em.parameter.value("data")=="on" && !Input) {
				setPinValue(true);
				checkRules(em); //Ex: If Node = on, delay 10s, turn off again
				emit changed(em);
			}
			else if(em.parameter.value("data")=="off" && !Input){
				setPinValue(false);
				checkRules(em);
				emit changed(em);
			}
		}
	} 
	else { //Notification about event from other node
		checkRules(em);
	}
}

QString SH_Node_GPIO::getWebHomeData()
{
    QString res;

    if(!initDone)
        initGPIO();

    getPinValue();

    if(!State)
        res = "<span class=\"ioOff ioStatus\"></span>";
    else
        res = "<span class=\"ioOn ioStatus\"></span>";

    if(!Input) {
        if(!State)
            res = "<a href=\"/api/sendEvent?node=" + QString::number(ID) + "&data=on\" class=\"ioButton catch\">" + res + "</a>";
        else
            res = "<a href=\"/api/sendEvent?node=" + QString::number(ID) + "&data=off\" class=\"ioButton catch\">" + res + "</a>";
    }

    return res + " " + this->Name;
}
