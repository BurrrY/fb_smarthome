#ifndef SH_Node_SI7021_H
#define SH_Node_SI7021_H
#include "fb_sh_lib_global.h"

#include "sh_node_sensor.h"

#include <QTimer>



class FB_SH_LIBSHARED_EXPORT  SH_Node_SI7021 : public SH_Node_Sensor
{
public:
    SH_Node_SI7021(sh_db *db);

    int getUpdateIntervalSec() const;
    void setUpdateIntervalSec(int value);


    QDateTime getLatestMeasurement() const;

    // Virtual Methods
    QJsonObject getJsonConfig();
    void setJsonConfig(QJsonObject jsonData);


    float getLatestTemp() const;
    float getLatestHumi() const;

private slots:
    void getData();
    void gotTempResponse(QNetworkReply *reply);
private:
    QTimer pollTimer;
    int updateIntervalSec;
    bool requestRunning;
    int latestTemp;
    int latestHumi;
    QDateTime latestMeasurement;

    // SH_Node_Sensor interface
public:
    QString getIcon();
    QString getText();
    QString getSubText();
};

#endif // SH_Node_SI7021_H
