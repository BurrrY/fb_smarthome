#include "sh_node_openclose.h"

SH_Node_OpenClose::SH_Node_OpenClose(sh_db *db) :
    SH_Node_Network(db)
{
    GroupType = Network;

}


void SH_Node_OpenClose::receiveEvent(SH_EventMessage em)
{
	qWarning() << "SH_Node_OpenClose::receiveEvent not implemented! ";
	/*
    qDebug() << "Switched Nodestate: " << action;
    bool ok;
    int actionID = action.toInt(&ok);

    if(!ok)
        return false;

    if(actionID >= SH_Node::NodeStates->length())
        return false;


    if(actionID == SH_Node::NodeStates->indexOf("Open")) {
        this->State = Open;
        db->insertSwitchHistory(ID, (int)State);
        switchHistory.insert(QDateTime::currentDateTime(),SwitchHistory(QDateTime::currentDateTime(), State));
        //checkRules(em);
    }
    else if(actionID == SH_Node::NodeStates->indexOf("Close"))   {
        this->State = Close;
        db->insertSwitchHistory(ID, (int)State);
        switchHistory.insert(QDateTime::currentDateTime(),SwitchHistory(QDateTime::currentDateTime(), State));
    //    checkRules(em);
    }
    else
        return false;

    return true;
	*/
}


QJsonObject SH_Node_OpenClose::getJsonConfig()
{
    QJsonObject json(SH_Node_Network::getJsonConfig());

    QJsonDocument doc(json);
    return json; //.toJson(QJsonDocument::Compact);
}

void SH_Node_OpenClose::setJsonConfig(QJsonObject jsonData)
{
    SH_Node_Network::setJsonConfig(jsonData);

}
