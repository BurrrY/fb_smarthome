#ifndef SH_DB_H
#define SH_DB_H
#include "fb_sh_lib_global.h"


#include <QFile>
#include <QFileInfo>
#include <QHostAddress>
#include <fb_sql.h>
#include <fb_sqlite.h>


#include "sh_logging.h"

class  SH_Node;

class FB_SH_LIBSHARED_EXPORT  sh_db : public FB_SQLite
{
public:
    sh_db();
    bool checkDB();
    static sh_db *getInstance();
    static void setInstance(sh_db *value);

//Nodes	
    bool updateNode(QString Name, int NodeID, int type, QJsonObject jsonData);
    bool createNode(QString Name, int typeID);

//Node-Data
    bool insertSwitchHistory(int nodeID, int State);
    DB_DataSet loadSwitchHistory(int NodeID);	
    bool insertAlert(int nodeID, int level, QString msg);	
    bool insertSolarData(int pwr, int ID);
    bool saveTemp(int val, int nodeID);
    bool saveSI7021(int tempVal, int humiVal, int nodeID);
};


#endif // SH_DB_H
