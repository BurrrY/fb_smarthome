#ifndef RULECONDITION_H
#define RULECONDITION_H
#include "fb_sh_lib_global.h"
#include "sh_eventmessage.h"
#include "sh_logging.h"
#include "sh_node_num.h"

#include <QString>
#include <QTime>


class FB_SH_LIBSHARED_EXPORT  RuleCondition
{
public:
    RuleCondition();
	
    RuleCondition(int srcID, QString cmp, QString data) {
 //       qCInfo(shRulesConditions) << ("Calling RuleCondition(srcID, cmp, data)");
		if(srcID<=0) {
  //          qCDebug(shRulesConditions) << "Tried to set invalid sourceNodeID:" << srcID ;
			sourceNodeID = -1;
		}
		else {			
			sourceNodeID = srcID;
		}
		Data = data;
		setComparator(cmp);
	}
	
    bool isValid(SH_EventMessage em);	
	int sourceNodeID;
	QString Data;
	QString getCompareResult();
	QString getComparator();
	void setComparator(const QString &cmp);
	
private:
	QString compareResult;
    QString Comparator;
};

#endif // RULECONDITION_H
