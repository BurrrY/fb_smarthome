#include "sh_node_OnOff.h"
#include "sh_node_lm75.h"
#include "webserver.h"
#include <fb_timespan.h>
#include "SH_WebReqRes.h"
#include "sh_node_gpio.h"
#include "sh_node_si7021.h"
#include "sh_node_solar.h"
#include "webserver_request.h"

#include <QTcpSocket>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QUrl>

int Webserver::BytesWritten = 0;

Webserver::Webserver(sh_db *Database, qint64 serverStartMSec)
{
    Server = new QTcpServer();
    DB = Database;
    BytesWritten = 0;
    serverStart = serverStartMSec;

    QObject::connect(Server, SIGNAL(newConnection()), this, SLOT(acceptConnection()));

    QFile fPageFoot(QDir::currentPath() + "/Serverpage/pageFoot.html");
    fPageFoot.open(QIODevice::ReadOnly);
    pageFoot = fPageFoot.readAll();

    QFile fPageHead(QDir::currentPath() + "/Serverpage/pageHead.html");
    fPageHead.open(QIODevice::ReadOnly);
    pageHead = fPageHead.readAll();

    if(!Server->listen(QHostAddress::Any, 8080))
   {
       qDebug() << "Server could not start";
   }
   else
   {
       qDebug() << "Server started!";
   }
}

Webserver::~Webserver()
{
    if(Server->isListening())
        Server->close();

    delete Server;
}

int Webserver::getKBytesWritten()
{
    return BytesWritten/1000;
}

void Webserver::receivedSocketData() {

}

void Webserver::acceptConnection()
{
    qCInfo(shLibWeb) << "New connection to server...";
	
	QTcpSocket *socket = Server->nextPendingConnection();
	socket->waitForReadyRead(1000);

	
	QString buff = "";
	while(socket->bytesAvailable())
		buff.append(socket->readAll());

	
	QStringList tokens = buff.split(QRegExp("[ \r\n][ \r\n]*"));

	
	if (buff.length() > 0) {
		{
			if(tokens[0] == "GET") {
				handleGetrequest(socket, tokens[1]);
			}
			else if(tokens[0] == "POST") {
				handlePOSTRequest(socket, tokens[1], tokens[tokens.count()-1]);
			}
			else {
				qDebug() << "Unhandeld Request: ---------";
				for (QList<QString>::iterator i = tokens.begin(); i != tokens.end(); ++i)
					qDebug() << (*i);
			}
		}
	}

	if(socket->isOpen()) {
		socket->waitForBytesWritten(3000);
		socket->close();
	}
}

void Webserver::writeToSocket(QTcpSocket *socket, QString data) {
    socket->write(data.toStdString().c_str());
    BytesWritten += data.size();
}

void Webserver::writeToSocket(QTcpSocket *socket, QByteArray data) {
    socket->write(data);
    BytesWritten += data.size();
}

void Webserver::handlePOSTRequest(QTcpSocket *socket, QString request, QString postData) {
    qCInfo(shLibWeb) << "POST-Request on " << request;

    if(request.left(5) == "/api/")
    {
        apiRequest(socket, request, postData);
    }
    else
    {
        writeToSocket(socket, QString("HTTP/1.0 404 Not Found\r\n\r\n"));
        writeToSocket(socket, QString("<h1>404 Not found</h1>Something not found"));
        qCCritical(shLibWeb) << "Error Handling POST-Request to: " << request << ". Data: "<< postData;
    }

}

void Webserver::handleGetrequest(QTcpSocket *socket, QString request) {
    qCInfo(shLibWeb) << "GET-Request on " << request;


    //Its an API-Request
    if(request.left(5) == "/api/")
    {
        apiRequest(socket,request, "");
    }
    //Webserver-UI
    else
    {
        QString getData;
        QMap<QString, QString> getDataMap;
        if(request.contains("?")) {
            int idx = request.indexOf("?");
            getData = request.right(request.length() - idx - 1);
            request = request.left(idx);
            getDataMap  = parsePostData(getData);
        }


        QString baseDir = QDir::currentPath() + "/Serverpage";
        QString requestedFilePath = baseDir + request;


        if(request.length() == 1)
            requestedFilePath += "index.html";

        QFile requestedFile(requestedFilePath);


        if(requestedFile.exists())
        {
            writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));

            requestedFile.open(QIODevice::ReadOnly);

            if(requestedFilePath.right(5) == ".html") {
                QString content = parseFile(&requestedFile, getDataMap);

                if(requestedFilePath.contains("/tpl/"))
                    writeToSocket(socket, content);
                else
                    writeToSocket(socket, addHTMLFrame(content));

            }
            else
                writeToSocket(socket, requestedFile.readAll());

            requestedFile.close();
        }
        else
        {
            qDebug() << "File '" + request + "' not found";
            writeToSocket(socket, QString("HTTP/1.0 404 Not Found\r\n\r\n"));
            writeToSocket(socket, "<h1>404 Not found</h1>File '" + request + "' not found");
        }
    }
}

QMap<QString, QString> Webserver::parsePostData(QString data)
{
    qCInfo(shLibWeb) << "calling parsePostData";

    QMap<QString, QString> postData;
    if(!data.isEmpty())
    {
        QRegExp rx("(\\w+)=([^\&]+)");
        data = data.replace('+', ' ');
        int pos = 0;
        while ((pos = rx.indexIn(data, pos)) != -1) {
            pos += rx.matchedLength();
            if(rx.captureCount() == 2) {
                postData.insert(rx.cap(1), rx.cap(2));
            }
        }
    }

    return postData;
}

QString Webserver::readFile(QString file) {

    QString baseDir = QDir::currentPath() + "/Serverpage/";
    QFile requestedFile(baseDir + file);
    if(!requestedFile.exists()) {
        qCCritical(shLibWeb) << "Requested File not found! :" << (baseDir + file);
    }
    else {
        qCInfo(shLibWeb) << "Read File: " << file << file.size();
    }

    requestedFile.open(QIODevice::ReadOnly);

    QString fileContent = requestedFile.readAll();
    requestedFile.close();

    return fileContent;
}

QString Webserver::apiRequest(QTcpSocket *socket, QString request, QString data) {

    QStringList validAPIPages;
    validAPIPages << "sendEvent" << "getAlert" << "saveNode" << "getHomeData";
    QString apiRequestOn = request.mid(5, request.length() - 1 - 4 );
    if(apiRequestOn.contains("?"))
        apiRequestOn = apiRequestOn.left(apiRequestOn.indexOf("?"));

    qCDebug(shLibWeb) << "API-Request on: " << apiRequestOn;

    QMap<QString, QString> postData;

    if(!data.isEmpty())
    {
        postData = parsePostData(data);
        qCDebug(shLibWeb) << postData;
    }else if (request.contains("?")) { //may there is some data in GET
        QString getData = request.right(request.size() - request.indexOf("?"));
        postData = parsePostData(getData);
    }

    if(!validAPIPages.contains(apiRequestOn))
    {
        writeToSocket(socket, QString("HTTP/1.0 404 Not Found\r\n\r\n"));
        writeToSocket(socket, QString("<h1>404 Not found</h1>Invalid API-Request-Page: " + apiRequestOn));
    }
    else
    {
        if(apiRequestOn == "sendEvent") {

            SH_EventMessage *em = new SH_EventMessage();
            em->parameter = postData;
            em->timestamp = QDateTime::currentDateTime();
            em->sender = -1;
            int nodeID = -1;
            bool convertOK;
			
            if(postData.contains("node")) {
                nodeID = postData.value("node").toInt(&convertOK);
                em->sender = nodeID;
            }
			else {
                qCWarning(shLibWeb) << "Cannot find NodeID!";
				writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
				writeToSocket(socket, SH_WebReqRes("sendEvent", "noNode", "", "No 'node' Parameter in POST-GET-Data found!").ToJson());
				return "ERROR";				
			}
            
            em->source = EventSourceComponent::ESC_Webserver;

            if(nodeID >= 0 && convertOK && Nodes->contains(nodeID)){
                Nodes->value(nodeID)->receiveEvent(*em);
            }
            else {
                qCWarning(shLibWeb) << "Cannot read NodeID!";
				writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
				writeToSocket(socket, SH_WebReqRes("sendEvent", "noValidNode", "", "No valid 'node' Parameter in POST-GET-Data found!").ToJson());
				return "ERROR";	
            }

			
            writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
			writeToSocket(socket, SH_WebReqRes("sendEvent", "none", "").ToJson());
        }
        else if(apiRequestOn == "saveNode") {
            //  QMap(("IP", "192.168.178.203")("Port", "80")("UpdateID", "5")("nodeID", "13")("nodeName", "WR+1")("nodeTypeDropdown", "5"))

            bool convertOK;
            QString nodeIDs = postData.value("nodeID", "!convertOK");
            int nodeID = nodeIDs.toInt(&convertOK);

            if((nodeIDs == "!convertOK" || !convertOK) && nodeIDs != "new") {
                qCWarning(shLibWeb) << "Cannot read NodeID!";
				writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
				writeToSocket(socket, SH_WebReqRes("saveNode", "noValidNode", "", "No valid 'node' Parameter in POST-GET-Data found!").ToJson());
                return "ERROR";
            }

            SH_Node *n = Nodes->value(nodeID, NULL);

            if(nodeIDs == "new") {
                n = new SH_Node(Nodes->first()->getDB());

            } else if(n == NULL) {
                qCWarning(shLibWeb) << "Cannot find node with ID " << nodeID << "!";
				writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
				writeToSocket(socket, SH_WebReqRes("saveNode", "noValidNode", "", "No valid 'node' Parameter in POST-GET-Data found!").ToJson());
                return "ERROR";
            }


            QJsonObject jo;

            for(int i=0;i<postData.count();++i)
                jo.insert(postData.keys()[i], QUrl::fromPercentEncoding( (postData.values()[i]).toLatin1() ) );


            n->setJsonConfig(jo);
            n->saveNode();


            writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));

            if(nodeIDs == "new")
                writeToSocket(socket, SH_WebReqRes("saveNode", "none", "/settings.html").ToJson());
            else
				writeToSocket(socket, SH_WebReqRes("saveNode", "none", "/settingsNodeOptions.html?ID=" + nodeIDs + "").ToJson());
        }
        else if(apiRequestOn == "getHomeData") {

            if(!postData.contains("node")) {
                writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
                writeToSocket(socket, QString("Error: Parameter 'node' not Provided!"));
                return "";
            }


            int nodeID = -1;
            bool convertOK;
            nodeID = postData.value("node").toInt(&convertOK);

            if(nodeID < 0 || !convertOK) {
                writeToSocket(socket, QString("HTTP/1.0 200 OK\r\n\r\n"));
                writeToSocket(socket, QString("Error: Parameter 'node' doesn't contain a valid node ID!"));
                return "";
            }





        }
    }

    return "";
}

QString Webserver::getAlertsSince(QString dateRequest) {
    //param like: latest=2016-01-28-20:07:56

    if(dateRequest.length() != 26)
        return "";

    dateRequest = dateRequest.mid(7, 10) + " " +  dateRequest.right(8);
    QString q = "SELECT Alerts.DateTime, Alerts.State, Nodes.Name FROM alerts, Nodes WHERE DateTime > \""
            + dateRequest + "\" AND Alerts.NodeID = Nodes.ID";

    DB_DataSet d = DB->getData(q);


    QJsonObject jsonRoot;
    QJsonArray jAlerts;

    for(DB_DataRow row : d)
    {
        QJsonObject jo;
        jo["nodeID"] = row["Name"].toString();
        jo["DateTime"] = row["DateTime"].toDateTime().toString("hh:mm");
        jo["State"] = row["State"].toString();
        jAlerts.append(jo);
    }


    QJsonDocument doc;
    jsonRoot["alerts"] = jAlerts;

    doc.setObject(jsonRoot);

    return doc.toJson();
}

QByteArray Webserver::parseFile(QFile *file, QMap<QString, QString> getData) {
    QByteArray d;

    if(!file->isOpen())
        file->open(QIODevice::ReadOnly);

    QString fileContent = file->readAll();
    file->close();


    //Scan for Frames
    if(fileContent.contains("%%FRAME:")) {
        int idxStart = fileContent.indexOf("%%FRAME:");
        int idxMid = fileContent.indexOf(":", idxStart);
        int idxEnd = fileContent.indexOf("%%", idxStart+2);

        QString frameName = fileContent.mid(idxMid+1, idxEnd-idxMid-1);
        QString framePath = QDir::currentPath() + "/Serverpage/tpl/frm_" + frameName + ".html";
        QFile frameFile(framePath);
        if(frameFile.exists()) {
            QString frameContent = readFile("/tpl/frm_" + frameName + ".html");
            fileContent = fileContent.replace(idxStart, idxEnd-idxStart+2, frameContent);
        }
        else {
            qCCritical(shLibWebConfig) << framePath << " does not exists";
        }
    }

    int vIdxStart = fileContent.indexOf("$$");
    while (vIdxStart >= 00)
    {
        int vIdxEnd = fileContent.indexOf(";", vIdxStart);
        if(vIdxEnd) {
            QString varName =fileContent.mid(vIdxStart+2, vIdxEnd-vIdxStart-2);
            QString varVal = loadVar(varName, getData);


            if(varVal != "empty!&%"){// && !varVal.isEmpty()) {

                qCInfo(shLibWeb) << "replacing var: " << varName;
                fileContent = fileContent.replace(vIdxStart, vIdxEnd-vIdxStart+1, varVal);
            }
            else {
                qCDebug(shLibWeb) << "  cannot replace var: " << varName;
            }
        }

        vIdxStart = fileContent.indexOf("$$", vIdxEnd);
    }

    d.append(fileContent);
    return d;
}

QString Webserver::loadVar(QString varName, QMap<QString, QString> data) {
    QString result = "empty!&%";

    if(varName=="SettingsNodeList") {
        //CHECKER
        QList<SH_Node *> nodeList = Nodes->values();

        //CHECKER
        for(SH_Node *n : nodeList )
        {
            QMap<QString, QString> tplData;
            tplData.insert("NAME", n->getName());
            tplData.insert("TEXT", n->getName() + " (" + n->getTypeName() + ")");
            tplData.insert("ID", QString::number(n->getID()));
            result.append(replaceTPL(tplData, getTemplate("SettingsNodeListEntry")));
        }
    }
    else if(varName=="nodeListPlugs") {
        result = loadOnOffNodes(SH_Node::TypeID("PowerPlug"));
    }
    else if(varName=="nodeListSwitch") {
        result = loadOnOffNodes(SH_Node::TypeID("Light"));
    }
    else if(varName=="nodeListOpenClose") {
        result = loadOpenCloseNodes("");
    }
    else if(varName=="nodeListSensors") {
        result = loadSensorNodes("");
    }
    else if(varName=="nodeListSolar") {
        result = loadSolarData("");
    }
    else if(varName=="allNodesAsOption") {
        for(SH_Node *n : Nodes->values())
            result += "<option value=\""+QString::number(n->getID())+"\">"+n->getName()+"</option>";
    }
    else if(varName=="allNodesAsOption") {
        result = ("");
    }
    else if(varName=="RuleData") {
        if(data.contains("ID")) {
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            if(n->getRules().length()>0)
                result = n->getRules().at(0)->Condition.Data;
            else
                result = "";
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID for " + varName;
        }
    }
    else if(varName=="RuleDelay") {
        if(data.contains("ID")) {
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            if(n->getRules().length()>0)
                result = QString::number(n->getRules().at(0)->Delay);
            else
                result = "";
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID for " + varName;
        }
    }
    else if(varName=="nodeEventsAsOption") {
        if(data.contains("ID")) {
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            for(QString event : n->getEventList())
                result += "<option>"+event+"</option>";
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID for " + varName;
        }
    }
    else if(varName=="uptime") {
        FB_Timespan s(QDateTime::currentMSecsSinceEpoch() - serverStart);
        result = s.toString("dd, hh:mm:ss");
    }
    else if(varName=="transfered") {
        result = getTransferedSize();
    }
    else if(varName=="database") {
        result = getDBStats();
    }
    else if(varName=="dbDownload") {
        result = "<a href=\"../FB_Smarthome.db\">Download</a>";
    }
    else if(varName=="currentDate") {
        result = QDate::currentDate().toString("dd. MMMM yyyy");
    }
    else if(varName=="currentTime") {
        result = QTime::currentTime().toString("hh:mm");
    }
    else if(varName=="dashboard") {
        result = loadDashboard();
    }
    else if(varName=="nodeName") {
        if(data.contains("ID")) {
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            result = n->getName();
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }
    }
    else if(varName=="ShowOnHome") {
        if(data.contains("ID")) {


            //CHECKER
            SH_Node *n = Nodes->value(data.value("ID").toInt());

            if(n->getShowOnHome())
                result = "checked";
            else
                result = "";
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }
    }
    else if(varName=="nodeCategory") {
        if(data.contains("ID")) {

            SH_Node *n = Nodes->value(data.value("ID").toInt());
            result = n->getHomeCategory();
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }
    }
    else if(varName=="nodeID") {
        if(data.contains("ID")) {
            result = data.value("ID");
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }
    }
    else if(varName=="nodeSettings") {
        if(data.contains("ID")) {

            //CHECKER
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            result = n->getName();
        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }
    }
    else if(varName=="nodeTypeDropDown") {
        if(data.contains("ID")) {


            //CHECKER
            SH_Node *n = Nodes->value(data.value("ID").toInt());

            for(int i=0; i < SH_Node::NodeTypes->length();++i) {
                QString tpl = getTemplate("NodeTypeDropdownEntry");
                QMap<QString, QString> tplData;
                tplData.insert("typeID", QString::number(i));
                tplData.insert("typeName", SH_Node::NodeTypes->value(i));
                if(n->getTypeID() == i)
                    tplData.insert("selected", "selected");
                else
                    tplData.insert("selected", "");

                result += replaceTPL(tplData, tpl);
            }
        } else {
            for(int i=0; i < SH_Node::NodeTypes->length();++i) {
                QString tpl = getTemplate("NodeTypeDropdownEntry");
                QMap<QString, QString> tplData;
                tplData.insert("typeID", QString::number(i));
                tplData.insert("typeName", SH_Node::NodeTypes->value(i));
                tplData.insert("selected", "");

                result += replaceTPL(tplData, tpl);
            }
        }
    }
    else if(varName=="nodeOptionsAddition") {

        if(data.contains("ID")) {

            //CHECKER
            SH_Node *n = Nodes->value(data.value("ID").toInt());
            result = getTemplate("NodeSettingsCommon");
            /*
            "Undefined",
            "Light",
            "PowerPlug",
            "OpenClose",
            "Reporter",
            "Solar",
            "Temp-Sens (LM75)",
            "Env-Sens (SI7021)",
            "RPi - GPIO"
            */

            if(n->getTypeID() == SH_Node::NodeTypes->indexOf("RPi - GPIO")
                    || n->getTypeID() == SH_Node::NodeTypes->indexOf("S0-Sensor")) {
                SH_Node_GPIO *n2 = (SH_Node_GPIO*)n;
                QMap<QString, QString> tplD;
                tplD.insert("PinNo", QString::number(n2->getPin()));
                tplD.insert("PinCheckInterval", QString::number(n2->getInputCheckInterval()));
                result += replaceTPL(tplD, getTemplate("NodeSettingsGPIO"));

            }
            else {
                SH_Node_Network *n2 = (SH_Node_Network*)n;
                QMap<QString, QString> tplD;
                tplD.insert("nodeIP", n2->getIP().toString());
                tplD.insert("nodePort", QString::number(n2->getPort()));
                result += replaceTPL(tplD, getTemplate("NodeSettingsNetwork"));
            }


            if(n->getTypeID() == SH_Node::NodeTypes->indexOf("Temp-Sens (LM75)")) {
                SH_Node_LM75 *n2 = (SH_Node_LM75*)n;
                QMap<QString, QString> tplD;
                tplD.insert("nodeAddrHexLM75", n2->getLM75Address());
                tplD.insert("nodeUpdateInterval", QString::number(n2->getUpdateIntervalSec()));
                result += replaceTPL(tplD, getTemplate("NodeSettingsLM75"));
            }


            if(n->getTypeID() == SH_Node::NodeTypes->indexOf("Solar")) {
                SH_Node_Solar *n2 = (SH_Node_Solar*)n;
                QMap<QString, QString> tplD;
                tplD.insert("nodeUpdateInterval", QString::number(n2->getUpdateInterval()));
                result += replaceTPL(tplD, getTemplate("NodeSettingsSolar"));
            }

        } else {
            qCCritical(shLibWeb) << "Missing Node-ID!";
        }

        return result;
    }

    return result;

}

QString Webserver::loadDashboard()
{

    //CHECKER
    QList<SH_Node *> list = Nodes->values();
    QMap<QString, QString> result;
    QString content = "";

    //fetch data

    //CHECKER
    for (SH_Node *node : list) {
        if(node->getShowOnHome())
        {
            QString category = node->getHomeCategory();
            if(!result.contains(category))
                result.insert(category, "");

            result[category] += "<p>"+node->getWebHomeData()+"</p>";
        }
    }

    //generate output


    for (QString key :  result.keys()) {
        content += "<section class=\"homeCategory\"><span class=\"categoryTitle\">"+key+"</span>"+result[key]+"</section>";
   }


    return content;
}

// Load Page-Content

QString Webserver::getTransferedSize() {

    float kb = (float)getKBytesWritten();
    QString unit = "KB";
    if(kb > 1000) {
        kb = kb / 1000;
        unit = "MB";
    }

    return QString::number(kb) + " " + unit;
}

QString Webserver::getDBStats() {
    int cntTempLog = DB->getData("SELECT count(*) as num FROM TempLog")[0]["num"].toInt();
    int cntSolarLog = DB->getData("SELECT count(*) as num FROM SolarLog")[0]["num"].toInt();
    int dbSize =  QFile("FB_Smarthome.db").size()/1000;

    QString txt = "TemperatureLog: " + QString::number(cntTempLog) + " Entries<br>";
    txt += "SolarLog: " + QString::number(cntSolarLog) + " Entries<br>";
    txt += "Database : " + QString::number(dbSize) + " kb";

    return txt;

}

QString Webserver::loadSensorNodes(QString getData) {
    if(getData.length() <= 0) {
        QString result = "";

        QString tpl = getTemplate("longButton");

        //CHECKER
        QList<SH_Node *> NodeList = Nodes->values();

        for(SH_Node *n : NodeList) {
            int nID = n->getTypeID();
            int tmpID = SH_Node::NodeTypes->indexOf("Temp-Sens (LM75)");
            int tmpID2 = SH_Node::NodeTypes->indexOf("Env-Sens (SI7021)");
            if(nID == tmpID || nID == tmpID2) {
                SH_Node_Sensor *tmpNode =  (SH_Node_Sensor*)n;

                QString nodeHTML = tpl;

                QMap<QString, QString> tplValues;

                tplValues.insert("head", tmpNode->getName());

                tplValues.insert("icon", tmpNode->getIcon());

                tplValues.insert("link", "temp.html?node=" + QString::number(tmpNode->getID()));
                tplValues.insert("text", tmpNode->getText());
                tplValues.insert("subText", tmpNode->getSubText());


                result += replaceTPL(tplValues, nodeHTML);

            }
        }

        return result;
    }
    else //get SingleNode
    {
        QString chartSeries = "{name: '$name;',color: '$color;',type: 'spline',dataGrouping: {approximation: \"average\"}, data: [$data;], lineWidth: 3 }";
        QString result = "";
        QRegExp rx("node=(\\d+)");
        rx.indexIn(getData);
        int ID = rx.cap(1).toInt();

        int nodeTypeID = Nodes->value(ID)->getTypeID();

        QFile mainTplFile(QDir::currentPath() + "/Serverpage/tempDetail.html");
        mainTplFile.open(QIODevice::ReadOnly);
        QString mainTpl = mainTplFile.readAll();
        mainTplFile.close();

        QDateTime start = QDateTime::currentDateTime();
        QDateTime end = start.addDays(-30);

        QString chartData = "";
        QString chartDataB = "";

        if(nodeTypeID == SH_Node::TypeID("Temp-Sens (LM75)")) {
            DB_DataSet d = DB->getData(QString("SELECT Timestamp, Value FROM TempLog WHERE ") +
                                   "Timestamp < \"" + sh_db::SQL_DATEFORMAT(start) + "\" and Timestamp > \"" + sh_db::SQL_DATEFORMAT(end) +
                                   "\" AND NodeID = " + QString::number(ID)+ " ORDER BY Timestamp asc");

            for(DB_DataRow row : d) {
                QDateTime t = row["Timestamp"].toDateTime();
                QString l = QString::number(t.toMSecsSinceEpoch());

                QString d =  QString::number(row["Value"].toInt());
                chartData += "[" + l + "," + d + "], ";
            }
            chartData = replaceTPL("data", chartData, chartSeries);
            chartData = replaceTPL("name", "Temperature", chartData);
            chartData = replaceTPL("color", "#6ac6fc", chartData);

            result = replaceTPL("series", chartData, mainTpl);
        }
        else {
            DB_DataSet d = DB->getData(QString("SELECT Timestamp, TempValue, HumiValue FROM SI7021Log WHERE ") +
                                   "Timestamp < \"" + sh_db::SQL_DATEFORMAT(start) + "\" and Timestamp > \"" + sh_db::SQL_DATEFORMAT(end) +
                                   "\" AND NodeID = " + QString::number(ID)+ " ORDER BY Timestamp asc");

            for(DB_DataRow row : d) {
                QDateTime t = row["Timestamp"].toDateTime();
                QString l = QString::number(t.toMSecsSinceEpoch());

                QString d =  QString::number(row["TempValue"].toInt()/100);
                QString d2 =  QString::number(row["HumiValue"].toInt()/100);
                chartData += "[" + l + "," + d + "], ";
                chartDataB += "[" + l + "," + d2 + "], ";

            }
            chartData = replaceTPL("data", chartData, chartSeries);
            chartData = replaceTPL("name", "Temperature", chartData);
            chartData = replaceTPL("color", "#6ac6fc", chartData);

            chartDataB = replaceTPL("data", chartDataB, chartSeries);
            chartDataB = replaceTPL("name", "Humidity", chartDataB);
            chartDataB = replaceTPL("color", "#f4bb00", chartDataB);

            result = replaceTPL("series", chartData + "," + chartDataB, mainTpl);
        }


        return result;
    }

}

QString Webserver::loadSolarData(QString getData) {

    QString legendA = "";
    QString legendB = "";
    QString strDataA = "";
    QString strDataB = "";
    QList<int> dataTmp;
    int requestedDays = 1;
    int offsetDays = 0;

    QString file = getTemplate("SolarData");


    QDateTime start = QDateTime::currentDateTime();
    QDateTime end;

    QMap<int, QList<int>> logData;
    QMap<int, QList<QDateTime>> logLables;


    //?days=3&offset=3

    QRegExp rx("days=(\\d+)");
    if(rx.exactMatch(getData))
        requestedDays = rx.cap(1).toInt();
    else
         requestedDays = 30;


    rx.setPattern("days=(\\d+)&offset=(\\d+)");
    if(rx.exactMatch(getData)) {
        requestedDays = rx.cap(1).toInt();
        offsetDays = rx.cap(2).toInt();
    } else {
         offsetDays = 0;
    }

    start = start.addDays(offsetDays * -1);
    end = start.addDays(requestedDays * -1);


    DB_DataSet d = DB->getData(QString("SELECT Timestamp, W_Total as power, Source FROM solarlog WHERE ") +
                               "timestamp < \"" + sh_db::SQL_DATEFORMAT(start) + "\" and timestamp > \"" + sh_db::SQL_DATEFORMAT(end) +
                               "\" ORDER BY timestamp asc");
    for(DB_DataRow row : d) {
        QDateTime t = row["Timestamp"].toDateTime();
        int src = row["Source"].toInt();

        if(!logData.keys().contains(src)) {
            logData.insert(src, QList<int>());
            logLables.insert(src, QList<QDateTime>());
        }

        logData[src].append(row["power"].toInt());
        logLables[src].append(t);
    }

    dataTmp = logData.values().at(0);
    for(int i=0;i<dataTmp.length();++i){
        QString d =  QString::number(dataTmp.at(i));
        QString l = QString::number(logLables.values().at(0).at(i).toMSecsSinceEpoch());
        strDataA += "[" + l + "," + d + "], ";
    }

    dataTmp = logData.values().at(1);
    for(int i=0;i<dataTmp.length();++i){
        QString d =  QString::number(dataTmp.at(i));
        QString l = QString::number(logLables.values().at(1).at(i).toMSecsSinceEpoch());
        strDataB += "[" + l + "," + d + "], ";
    }

    file = replaceTPL("dataA", strDataA, file);
    file = replaceTPL("dataB", strDataB, file);

    return file;

}

QString Webserver::loadOnOffNodes(int typeID) {


    QString nodeData = "";

    for(int i=0;i<Nodes->count();++i) {
        if(Nodes->values().at(i)->getTypeID() == typeID) {

            SH_Node_OnOff *n = (SH_Node_OnOff*)(Nodes->values().at(i));
            QString nodeHTML = getTemplate("OnOffNode");

            QMap<QString, QString> tplValues;
            tplValues.insert("nodeID", QString::number(n->getID()));
            tplValues.insert("name", n->getName());

            switch(typeID) {
                case NodeType::PowerPlug:
                    tplValues.insert("page", "plugs");
                break;
            case NodeType::Light:
                    tplValues.insert("page", "light");
                break;
            default:
                    return "";
                break;
            }

			/*
            switch(n->getState()) {
                case NodeState::On:
                    tplValues.insert("action", "off");
                    tplValues.insert("icon", "power_on");
                break;
                case NodeState::Off:
                    tplValues.insert("action", "on");
                    tplValues.insert("icon", "power_off");
                break;
                case NodeState::UnknownState:
                    tplValues.insert("action", "on");
                    tplValues.insert("icon", "power_off");
                break;
            default:
                    return "";
                break;
            }
*/

            nodeData += replaceTPL(tplValues, nodeHTML);
        }

    }

    return nodeData;
}

QString Webserver::loadSwitchHistory(int NodeID) {
/*
TODO: fix this
    SH_Node *n = Nodes->value(NodeID);
    QString result = "<dl>\n";

    QMapIterator<QDateTime , SwitchHistory> i(n->switchHistory);
     i.toBack();
     while (i.hasPrevious()) {
         i.previous();
         SwitchHistory h = i.value();
         result.append("<dt>" + h.Timestamp.toString("dd.MM, hh:mm:ss") + "</dt>" +
                       "<dd class=\"Nodestate_" + SH_Node::StateToString(h.State) + "\">" + SH_Node::StateToString(h.State) + "</dd>\n");
     }


    result.append("</dl>\n");

    QFile nodeHTMLFile(QDir::currentPath() + "/Serverpage/switchHistory.html");
    nodeHTMLFile.open(QIODevice::ReadOnly);

    return replaceTPL("historyList", result, nodeHTMLFile.readAll());
*/
return "";
}

QString Webserver::loadOpenCloseNodes(QString getData) {

    QRegExp rx("history=(\\d+)");
    if(rx.exactMatch(getData))
        return loadSwitchHistory(rx.cap(1).toInt());

    QString nodeData = "";
    for(int i=0;i<Nodes->count();++i) {
        if(Nodes->values().at(i)->getTypeID() == SH_Node::TypeToID("OpenClose")) {
            SH_Node *n = Nodes->values().at(i);


            QString nodeHTML = getTemplate("OpenCloseNode");

            QMap<QString, QString> tplValues;
            tplValues.insert("nodeID", QString::number(n->getID()));
            tplValues.insert("name", n->getName());
            tplValues.insert("page", "light");
            tplValues.insert("link", "/openClose.html?history=" + QString::number(n->getID()));

/*
            switch(n->getState()) {
                case NodeState::Open:
                    tplValues.insert("icon", "window");
                break;
                case NodeState::Close:
                    tplValues.insert("icon", "windowClosed");
                break;
                case NodeState::UnknownState:
                    tplValues.insert("icon", "power_off");
                break;
            default:

                break;
            }
			*/
            nodeData += replaceTPL(tplValues, nodeHTML);
        }
    }

    return nodeData;
}

//File loading & manipulation

QString Webserver::getTemplate(QString tplName) {
    return readFile("tpl/tpl_" + tplName + ".html");
}

QString Webserver::replaceTPL(QString var, QString value, QString html) {
    return html.replace("$" + var + ";", value);
}

QString Webserver::replaceTPL(QMap<QString, QString> vars, QString html) {

    for(int i=0;i<vars.count();++i) {
        QString var = "$" +vars.keys().at(i) + ";";
        QString value = vars.values().at(i);
        html = html.replace(var, value);
    }

    return html;
}

QString Webserver::addHTMLFrame(QString content) {
    return pageHead + content + pageFoot;
}
