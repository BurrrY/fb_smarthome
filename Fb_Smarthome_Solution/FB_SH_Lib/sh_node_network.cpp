#include "sh_node_network.h"
#include "stathelp.h"

const QString SH_Node_Network::NODE_CMD_ON = "/gpio/2/1";
const QString SH_Node_Network::NODE_CMD_OFF = "/gpio/2/0";
const QString SH_Node_Network::NODE_GET_STATE = "/gpio/";


SH_Node_Network::SH_Node_Network()
{
    GroupType = Network;

}

SH_Node_Network::SH_Node_Network(sh_db *db) :
    SH_Node(db)
{
    IP = QHostAddress("0.0.0.0");
    Port = 0;
    GroupType = Network;

 //   connect(&nwManager, QNetworkAccessManager::finished, this, managerFinished);
}


QJsonObject SH_Node_Network::getJsonConfig()
{
    QJsonObject json(SH_Node::getJsonConfig());
    json.insert("IP", getIP().toString());
    json.insert("Port", getPort());



    QJsonDocument doc(json);
    return json;
}

void SH_Node_Network::setJsonConfig(QJsonObject jsonData)
{
    SH_Node::setJsonConfig(jsonData);

    if(jsonData.contains("IP"))
        setIP(jsonData.value("IP").toString());

    if(jsonData.contains("Port"))
        setPort(StatHelp::JsonToInt(jsonData.value("Port")));

}

bool SH_Node_Network::fetchData(QUrl url)
{

 //   nwRequest = new ;

    nwManager.get(QNetworkRequest(url));

    return true;
}

void SH_Node_Network::managerFinished(QNetworkReply *reply)
{
    qDebug() << requestReply->readAll();
    requestReply = reply;
}



QHostAddress SH_Node_Network::getIP() const
{
    return IP;
}

void SH_Node_Network::setIP(const QHostAddress &value)
{
    IP = value;
}

void SH_Node_Network::setIP(const QString &value)
{
    IP = QHostAddress(value);
}

void SH_Node_Network::setIP(const quint32 &value)
{
    IP = QHostAddress(value);
}

int SH_Node_Network::getPort() const
{
    return Port;
}

void SH_Node_Network::setPort(int value)
{
    Port = value;
}
