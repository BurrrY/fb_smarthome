#ifndef WEBSERVER_H
#define WEBSERVER_H

#include "sh_eventmessage.h"

#include <sh_db.h>
#include <sh_node.h>
#include <sh_node_network.h>

#include <QFile>
#include <QTcpServer>
#include <QTime>


class FB_SH_LIBSHARED_EXPORT Webserver : public QObject
{
    Q_OBJECT

public:
    Webserver(sh_db *Database, qint64 serverStartMSec);
    ~Webserver();

    //CHECKER
    QMap<int, SH_Node*> *Nodes;
    int getKBytesWritten();
private:
    QTcpServer *Server;
    qint64 serverStart;
    void handleGetrequest(QTcpSocket *socket, QString request);
    void handlePOSTRequest(QTcpSocket *socket, QString request, QString postData);


    //Nodes
    bool refreshOK;

    QString loadOnOffNodes(int typeID);
    QString loadOpenCloseNodes(QString getData);
    QString loadSolarData(QString getData);

    sh_db *DB;
    QString getDBStats();
    QString getTransferedSize();
    QString loadSwitchHistory(int NodeID);
    QString getAlertsSince(QString dateRequest);
    QString loadSensorNodes(QString getData);
    QString apiRequest(QTcpSocket *socket, QString request, QString data);
    QMap<QString, QString> parsePostData(QString data);

    //Web, Sockets...
    static int BytesWritten;
    void writeToSocket(QTcpSocket *socket, QString data);
    void writeToSocket(QTcpSocket *socket, QByteArray data);

    //File&HTML-Stuff
    QByteArray pageHead;
    QByteArray pageFoot;

    QString readFile(QString file);
    QString addHTMLFrame(QString content);

    //Dynamic Content
    QString loadVar(QString varName, QMap<QString, QString> data);
    QString loadDashboard();

    //TemplateStuff
    QString replacddeVars(QString file);
    QString replaceTPL(QString var, QString value, QString html);
    QString replaceTPL(QMap<QString, QString> vars, QString html);
    QString getTemplate(QString tplName);
    QByteArray parseFile(QFile *file, QMap<QString, QString> getData);
signals:
    void receiveEvent(SH_EventMessage msg);
    void reloadNodes();
public slots:
       void acceptConnection();
       void receivedSocketData();
};

#endif // WEBSERVER_H
