#ifndef STATHELP_H
#define STATHELP_H

#include <QJsonValue>
#include <QString>



class StatHelp
{
public:
    StatHelp();

    static int JsonToInt(QJsonValue val);
    static QString JsonToQString(QJsonValue val);
    static bool JsonToBool(QJsonValue val);
};

#endif // STATHELP_H
