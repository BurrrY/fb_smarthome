#ifndef MESSAGEBUS_H
#define MESSAGEBUS_H
#include "fb_sh_lib_global.h"

#include <QWebSocketServer>
#include <QWebSocket>

class FB_SH_LIBSHARED_EXPORT MessageBus : public QObject
{
    Q_OBJECT
public:
    explicit MessageBus(int port, bool debug = false, QObject *parent = Q_NULLPTR);
    ~MessageBus();
private:
    QWebSocketServer *pWebSocketServer;
    QList<QWebSocket *> clients;
    bool debug;

Q_SIGNALS:
    void closed();

private Q_SLOTS:
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void socketDisconnected();

public slots:
};

#endif // MESSAGEBUS_H
