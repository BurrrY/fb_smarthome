#include "sh_node.h"
#include "sh_node_OnOff.h"
#include "sh_node_lm75.h"
#include "sh_node_openclose.h"
#include "sh_node_s0.h"
#include "sh_node_si7021.h"
#include "sh_node_solar.h"
#include "stathelp.h"

QStringList *SH_Node::NodeTypes = new QStringList({
                                                      "Undefined",
                                                      "Light",
                                                      "PowerPlug",
                                                      "OpenClose",
                                                      "Reporter",
                                                      "Solar",
                                                      "Temp-Sens (LM75)",
                                                      "Env-Sens (SI7021)",
                                                      "RPi - GPIO",
                                                      "S0-Sensor"
                                                  }) ;

SH_Node::SH_Node()
{
    Name = ("New Node");
    ID = -1;
    typeID = 0;
    GroupType = Other;
    showOnHome = false;
    homeCategory = "";
}

SH_Node::SH_Node(sh_db *db)
{
    Name = ("New Node");
    ID = -1;
    typeID = 0;
    GroupType = Other;
    this->db = db;
    showOnHome = false;
    homeCategory = "";
}

SH_Node::~SH_Node()
{

}

SH_Node* SH_Node::nodeFactory(int nodeType, sh_db *db)
{
	
  if(nodeType == SH_Node::TypeID("Light") ||
     nodeType == SH_Node::TypeID("PowerPlug")) 
      return ( new SH_Node_OnOff(db) );
  else if(nodeType == SH_Node::TypeID("OpenClose")) 
      return new SH_Node_OpenClose(db);
  else if(nodeType == SH_Node::TypeID("Temp-Sens (LM75)")) 
      return new SH_Node_LM75(db);
  else if(nodeType == SH_Node::TypeID("Env-Sens (SI7021)"))
      return new SH_Node_SI7021(db);
  else if(nodeType == SH_Node::TypeID("Solar"))
      return new SH_Node_Solar(db);
  else if(nodeType == SH_Node::TypeID("S0-Sensor")) 
      return new SH_Node_S0(db);
  else if(nodeType == SH_Node::TypeID("RPi - GPIO"))
      return new SH_Node_GPIO(db);
  else 
      return new SH_Node(db);  

}


QString SH_Node::getName() const
{
    return Name;
}

void SH_Node::setName(const QString &value)
{
    Name = value;
}

int SH_Node::getID() const
{
    return ID;
}

void SH_Node::setID(int value)
{
    ID = value;
}

bool SH_Node::saveNode()
{
    if(ID < 0) //Check if the code already exists
        return db->createNode(Name, typeID);

    return db->updateNode(Name, ID, typeID, getJsonConfig());
}

QList<Rule *> SH_Node::getRules() const
{
    return rules;
}

QString SH_Node::getHomeCategory() const
{
    if(homeCategory==NULL)
        return "";
    else
        return homeCategory;
}

void SH_Node::setHomeCategory(QString value)
{
    homeCategory = value;
}

bool SH_Node::getShowOnHome() const
{
    return showOnHome;
}

void SH_Node::setShowOnHome(bool value)
{
    showOnHome = value;
}



void SH_Node::setBasicInfo(DB_DataRow r)
{
    setID(r.value("ID").toInt());
    setName(r.value("Name").toString());
    setTypeID(r.value("Type").toInt());
 //   setState(r.value("LatestState").toString());
}

//TODO: Implement this for NodeTypes
// Show on HomePage, add Categories

QString SH_Node::getWebHomeData()
{
    return Name;
}

NodeGroup SH_Node::getGroupType() const
{
    return GroupType;
}

void SH_Node::checkRules(SH_EventMessage em)
{	
    for(Rule *r : rules)
        if(r->Condition.isValid(em))
            receiveEvent(r->Action);

}


void SH_Node::addRule(Rule* r) {
    rules.append(r);
}

void SH_Node::receiveEvent(SH_EventMessage em)
{

}

sh_db* SH_Node::getDB() const
{
    return db;
}


QJsonObject SH_Node::getJsonConfig()
{
    QJsonObject json;
    json.insert("ShowOnHome", showOnHome);
    json.insert("homeCategory", homeCategory);
	
    return json;
}

void SH_Node::setJsonConfig(QJsonObject jsonData)
{
   if(jsonData.contains("nodeName"))
       setName(jsonData.value("nodeName").toString());

   if(jsonData.contains("nodeTypeDropdown"))
       setTypeID(StatHelp::JsonToInt(jsonData.value("nodeTypeDropdown")));

   if(jsonData.contains("ShowOnHome"))
       setShowOnHome(StatHelp::JsonToBool(jsonData.value("ShowOnHome")));

   if(jsonData.contains("homeCategory"))
       setHomeCategory(jsonData.value("homeCategory").toString());
   
}

 QStringList SH_Node::getEventList() {
   QStringList events;


   return events;
 }


int SH_Node::getTypeID() const
{
    return typeID;
}


void SH_Node::setTypeID(int value)
{
    typeID = value;
}


int SH_Node::TypeToID(QString type)
{
    if(NodeTypes->contains(type))
        return NodeTypes->indexOf(type);
    else
        return -1;
}

int SH_Node::TypeID(QString type)
{
    return TypeToID(type);
}

QString SH_Node::getTypeName() const
{
    return TypeToString(typeID);
}

QString SH_Node::TypeToString(int typeID)
{

    if(typeID > SH_Node::NodeTypes->length()-1 )
    {
        qWarning() << "NodeType not Found! :" << typeID;
        return "";
    }
    else
        return SH_Node::NodeTypes->at(typeID);
}
