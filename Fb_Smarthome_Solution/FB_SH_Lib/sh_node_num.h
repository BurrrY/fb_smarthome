#ifndef SH_NODE_NUM_H
#define SH_NODE_NUM_H
#include "fb_sh_lib_global.h"

#define CFG_USE_PB  "usePushBulletGlobal"
#define CFG_PB_Token "PushbulletToken"


enum NodeGroup  {
    Network,
    GPIO,
    Other
};

enum NodeState  {
    On,
    Off,
    UnknownState,
    Open,
    Close
};

enum NodeType  {
    Undefined,
    Light,
    PowerPlug,
    OpenClose,
    Reporter,
    Solar,
    Temperatur
};



#endif // SH_NODE_NUM_H
