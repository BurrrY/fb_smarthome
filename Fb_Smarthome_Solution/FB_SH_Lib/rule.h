#ifndef RULE_H
#define RULE_H
#include "fb_sh_lib_global.h"

#include "ruleaction.h"
#include "rulecondition.h"
#include "sh_eventmessage.h"

#include <QList>
#include <QString>

enum RuleConditionType {
    Compare

};


class FB_SH_LIBSHARED_EXPORT  Rule
{
public:

    Rule();
	
    Rule(QString RuleName) {
		Name = RuleName;
        Delay = 0;
	}
	
    QString Name;	
    int Delay;
	SH_EventMessage Action;
	RuleCondition Condition;
};

#endif // RULE_H
